/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#define PYTHON_DEBUG true

#include <pybind11/include/pybind11/pybind11.h>
#include <pybind11/include/pybind11/embed.h>
#include <pybind11/include/pybind11/numpy.h>


#include "PyLogHook.h"

#include "PythonInterpreter.hh"

#include <QMetaObject>
#include <QMetaMethod>

#include "PythonTypeConversions.hh"

#include <OpenFlipper/BasePlugin/PythonFunctionsCore.hh>

namespace py = pybind11;

static Core* core_;

static pybind11::module mainModule_;

static PythonInterpreter* interpreter_ = nullptr;

/** \brief Stores a clean dictionary for interpreter reset
 *
 */
static PyObject* global_dict_clean_ = nullptr;


void setCorePointer( Core* _core ) {
  core_ = _core;
}

PythonInterpreter* PythonInterpreter::getInstance() {

  if (interpreter_ == nullptr)
  {
    interpreter_ = new PythonInterpreter();
  }

  return interpreter_;

}

PythonInterpreter::PythonInterpreter() :
    externalLogging_(true)
{

}

PythonInterpreter::~PythonInterpreter() {

}

bool PythonInterpreter::modulesInitialized() {
  ///@TODO: Why does this work?
  return static_cast<bool>(mainModule_);
}

void PythonInterpreter::initPython() {

  if (Py_IsInitialized()  ) {
    #ifdef PYTHON_DEBUG
      std::cerr << "Python already Initialized!" << std::endl;
    #endif
    return;
  }

  #ifdef PYTHON_DEBUG
    std::cerr << "Initialize interpreter ... " ;
  #endif

  py::initialize_interpreter();

  #ifdef PYTHON_DEBUG
     std::cerr << " Done" << std::endl;
     std::cerr << "Initialize Threads ...";
  #endif

  PyEval_InitThreads();

  #ifdef PYTHON_DEBUG
     std::cerr << " Done" << std::endl;
  #endif

  if (!modulesInitialized()) {
    #ifdef PYTHON_DEBUG
      std::cerr << "Import __main__" ;
    #endif

    //dlopen("libpython3.5m.so.1.0", RTLD_LAZY | RTLD_GLOBAL);
    py::module main{ py::module::import("__main__") };

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
      std::cerr << "Redirect Outputs ...";
    #endif

    // Redirect python output to integrated logger functions
    tyti::pylog::redirect_stderr([this](const char*w) {this->pyError(w); });
    tyti::pylog::redirect_stdout([this](const char* w) {this->pyOutput(w); });

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
      std::cerr << "Get __dict__ from  main namespace ...";
    #endif

    // =========================================================
    // Add OpenFlipper Core module to main namespace
    // =========================================================
    py::object main_namespace = main.attr("__dict__");

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
      std::cerr << "Importing OpenFlipper core module ...";
    #endif

    py::module of_module(py::module::import("openflipper"));
    main_namespace["openflipper"] = of_module;

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
    #endif

    // =========================================================
    // Import with python interface to main namespace
    // =========================================================

    QStringList pythonPlugins = getPythonPlugins();

    for ( int i = 0 ; i < pythonPlugins.size() ; ++i ) {

      #ifdef PYTHON_DEBUG
        std::cerr << "Importing "+ pythonPlugins[i].toStdString() + " module ...";
      #endif

      py::module om_module(py::module::import(pythonPlugins[i].toStdString().c_str()));
      main_namespace[pythonPlugins[i].toStdString().c_str()] = om_module;

      #ifdef PYTHON_DEBUG
        std::cerr << " Done" << std::endl;
      #endif
    }

    #ifdef PYTHON_DEBUG
      std::cerr << "Copy dict ...";
    #endif

    global_dict_clean_ = PyDict_Copy(PyModule_GetDict(main.ptr()));

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
      std::cerr << "Set to validate state ...";
    #endif

    // set the main module member to a validate state, shows, that all modules are successfully initialized
    mainModule_ = std::move(main);

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
      std::cerr << "Save thread ...";
    #endif

    // Do not release the GIL until all modules are initalzed
    PyEval_SaveThread();

    #ifdef PYTHON_DEBUG
      std::cerr << " Done" << std::endl;
    #endif


  }

}

void PythonInterpreter::resetInterpreter()
{
    if (!Py_IsInitialized() || !modulesInitialized())
        return;

    PyGILState_STATE state = PyGILState_Ensure();
    PyObject* dict = PyModule_GetDict(mainModule_.ptr());
    PyDict_Clear(dict);
    PyDict_Update(dict, global_dict_clean_);
    PyGILState_Release(state);
}


bool PythonInterpreter::runScript(QString _script) {

#ifdef PYTHON_DEBUG
  std::cerr << "runScript" << std::endl;
#endif

  //============================================================
  // Prepend module instance getter to the script
  //============================================================

  _script.prepend("core = openflipper.Core()\n");

  QStringList pythonPlugins = getPythonPlugins();

  for ( int i = 0 ; i < pythonPlugins.size() ; ++i ) {
      QString import = pythonPlugins[i].toLower() + " = " + pythonPlugins[i] + "." + pythonPlugins[i] + "()\n";
      _script.prepend(import);
  }

  // Try to initialize python system
  try
  {

    #ifdef PYTHON_DEBUG
      std::cerr << "Initialize Python" << std::endl;
    #endif

    initPython();

    #ifdef PYTHON_DEBUG
      std::cerr << "Done initializing Python" << std::endl;
    #endif
  }
  catch (py::error_already_set &e)
  {
    pyError(e.what());
    return false;
  }

  PyGILState_STATE state = PyGILState_Ensure();
  //PyThreadState* tstate = PyGILState_GetThisThreadState();

  auto locals = mainModule_.attr("__dict__");

  bool result = true;

  try
  {

    std::cerr << "Now executing script:" << std::endl;
    std::cerr << _script.toStdString() << std::endl;

    py::exec(_script.toStdString(), py::globals(), locals);

    std::cerr << "Finished successfully" << std::endl;
  }
  catch (py::error_already_set &e)
  {
    pyError(e.what());
    e.restore();
    result = false;
  }
  catch (const std::runtime_error &e)
  {
    pyError(e.what());
    pyOutput("Restarting Interpreter.");
    resetInterpreter();
    result = false;
    state = PyGILState_Ensure();
  }

  PyGILState_Release(state);

  return result;
}

QString PythonInterpreter::runScriptOutput(QString _script) {
  LogOut.clear();
  LogErr.clear();
  externalLogging_ = false;
  runScript(_script);
  externalLogging_ = true;
  return LogOut + LogErr;
}


// Python callback functions
void PythonInterpreter::pyOutput(const char* w)
{
  if (externalLogging_) {
    emit log(LOGOUT, QString(w));
  } else {
    LogOut += QString::fromUtf8(w);
  }
}

void PythonInterpreter::pyError(const char* w)
{
  if (externalLogging_) {
    if (OpenFlipper::Options::nogui()) {
      std::cerr << "Python Error! " << w << std::endl;
      exit(1);
    }
    emit log(LOGERR, QString(w));
  } else {
    LogErr += QString::fromUtf8(w);
  }
}




PYBIND11_EMBEDDED_MODULE(openflipper, m) {

  // Export our core. Make sure that the c++ worlds core objet is not deleted if
  // the python side gets deleted!!
  py::class_<Core,std::unique_ptr<Core, py::nodelete>> core(m, "Core");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  core.def(py::init([]() { return core_; }));

  core.def("updateView", &Core::updateView, QCoreApplication::translate("PythonDocCore","Redraw the contents of the viewer.").toLatin1().data() );
  core.def("blockScenegraphUpdates", &Core::blockScenegraphUpdates, QCoreApplication::translate("PythonDocCore","Disable Scenegraph Updates (e.g. before loading or adding a large number of files)").toLatin1().data() );
  core.def("updateUI", &Core::updateUI, QCoreApplication::translate("PythonDocCore","Process events during script execution to keep the ui alive").toLatin1().data() );
  core.def("clearAll", &Core::clearAll, QCoreApplication::translate("PythonDocCore","Clear all data objects.").toLatin1().data() );
  core.def("deleteObject", &Core::deleteObject, QCoreApplication::translate("PythonDocCore","Delete an object from the scene.").toLatin1().data() ,
                                       py::arg( QCoreApplication::translate("PythonDocCore","Id of the object to delete.").toLatin1().data() ) );

  core.def("setObjectComment", &Core::setObjectComment, QCoreApplication::translate("PythonDocCore","Add a comment to an object (saved e.g. in screenshot metadata).").toLatin1().data()) ,
                                               py::arg( QCoreApplication::translate("PythonDocCore","Id of the object to add comment.").toLatin1().data()),
                                               py::arg( QCoreApplication::translate("PythonDocCore","Key value").toLatin1().data()),
                                               py::arg( QCoreApplication::translate("PythonDocCore","Actual comment").toLatin1().data());

  core.def("clearObjectComment", &Core::clearObjectComment, QCoreApplication::translate("PythonDocCore","Remove a comment from an object.").toLatin1().data()) ,
                                                   py::arg( QCoreApplication::translate("PythonDocCore","Id of the object to remove comment from.").toLatin1().data()),
                                                   py::arg( QCoreApplication::translate("PythonDocCore","Key value to remove").toLatin1().data());

  core.def("clearAllComments", &Core::clearObjectComment, QCoreApplication::translate("PythonDocCore","Remove all comments from an object.").toLatin1().data()) ,
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Id of the object to remove comments from.").toLatin1().data());

  core.def("fullscreen", &Core::fullscreen, QCoreApplication::translate("PythonDocCore","Enable or disable fullscreen mode.").toLatin1().data() ,
                                   py::arg( QCoreApplication::translate("PythonDocCore","Enable or disable?").toLatin1().data() ) );

  core.def("showViewModeControls", &Core::deleteObject, QCoreApplication::translate("PythonDocCore","Show or hide the view mode control box").toLatin1().data() ,
                                               py::arg( QCoreApplication::translate("PythonDocCore","Visible?").toLatin1().data() ) );

  core.def("loggerState", &Core::loggerState, QCoreApplication::translate("PythonDocCore","Change the logger window state").toLatin1().data() ,
                                     py::arg( QCoreApplication::translate("PythonDocCore","0 = In Scene , 1 = Normal, 2 = Hidden").toLatin1().data() ) );

  core.def("enableOpenMeshErrorLog", &Core::enableOpenMeshErrorLog, QCoreApplication::translate("PythonDocCore","Enable or disable OpenMesh error logging").toLatin1().data() ,
                                                           py::arg( QCoreApplication::translate("PythonDocCore","Enable or Disable").toLatin1().data() ) );

  core.def("showToolbox", &Core::showToolbox, QCoreApplication::translate("PythonDocCore","Show or hide toolbox").toLatin1().data() ,
                                     py::arg( QCoreApplication::translate("PythonDocCore","Visible?").toLatin1().data() ) );

  core.def("showStatusBar", &Core::showStatusBar, QCoreApplication::translate("PythonDocCore","Show or hide status bar").toLatin1().data() ,
                                         py::arg( QCoreApplication::translate("PythonDocCore","Visible?").toLatin1().data() ) );

  core.def("multiViewMode", &Core::multiViewMode, QCoreApplication::translate("PythonDocCore","Switch MultiView Mode").toLatin1().data() ,
                                         py::arg( QCoreApplication::translate("PythonDocCore","0: One Viewer, 1: Double Viewer, 2: Grid, 3: Horizontal split").toLatin1().data() ) );

  core.def("restrictFrameRate", &Core::restrictFrameRate, QCoreApplication::translate("PythonDocCore","Restrict maximal rendering FrameRate to MaxFrameRate").toLatin1().data() ,
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Restrict Framerate?").toLatin1().data() ) );

  core.def("setMaxFrameRate", &Core::setMaxFrameRate, QCoreApplication::translate("PythonDocCore","Set the maximal framerate (automatically enables framerate restriction)").toLatin1().data() ,
                                             py::arg( QCoreApplication::translate("PythonDocCore","Maximum frameRate").toLatin1().data() ) );

  core.def("snapshotBaseFileName", &Core::snapshotBaseFileName, QCoreApplication::translate("PythonDocCore","Set a base filename for storing snapshots. This setting is viewer dependent").toLatin1().data() ,
                                                       py::arg( QCoreApplication::translate("PythonDocCore","Base filename").toLatin1().data()),
                                                       py::arg( QCoreApplication::translate("PythonDocCore","Viewer ID to set the filename for").toLatin1().data() ) );

  core.def("snapshotFileType", &Core::snapshotFileType, QCoreApplication::translate("PythonDocCore","Set a filetype for storing snapshots.").toLatin1().data() ,
                                               py::arg( QCoreApplication::translate("PythonDocCore","Image type as string (e.g. jpg )").toLatin1().data()),
                                               py::arg( QCoreApplication::translate("PythonDocCore","Viewer ID to set the filetype for").toLatin1().data() ) );

  core.def("snapshotCounterStart", &Core::snapshotCounterStart, QCoreApplication::translate("PythonDocCore","Set the starting number for the snapshot counter.").toLatin1().data() ,
                                                       py::arg( QCoreApplication::translate("PythonDocCore","Starting number for the counter").toLatin1().data() ),
                                                       py::arg( QCoreApplication::translate("PythonDocCore","Viewer ID to set the counter for").toLatin1().data() ) );


  core.def("snapshot", &Core::snapshot, QCoreApplication::translate("PythonDocCore",   "Make a snapshot of the viewer with id viewerId.\n"
                                                                                       "Pass 0 as viewerId parameter to capture the current viewer. \n"
                                                                                       "The captured image will have the specified dimensions. \n"
                                                                                       "If 0 is passed as either width or height parameter, the value will \n"
                                                                                       "automatically be set to hold the right aspect ratio, respectively. \n"
                                                                                       "If 0 is passed for both width and height values, the viewport's current \n"
                                                                                       "dimension is used. Set alpha to true if you want the background to be transparent. \n"
                                                                                       "The fifth parameter is used to hide the coordinate system in the upper right corner of the screen. \n"
                                                                                       "If no filename was set using snapshotBaseFileName() the snapshot is stored \n"
                                                                                       "in snap.png in the current directory. For every snapshot \n"
                                                                                       "a counter is added to the filename.").toLatin1().data() ,
                               py::arg( QCoreApplication::translate("PythonDocCore","Id of viewer").toLatin1().data() ) = 0,
                               py::arg( QCoreApplication::translate("PythonDocCore","Width of image").toLatin1().data() )= 0,
                               py::arg( QCoreApplication::translate("PythonDocCore","Height of image").toLatin1().data() )= 0,
                               py::arg( QCoreApplication::translate("PythonDocCore","Transparent background?").toLatin1().data() ) = false,
                               py::arg( QCoreApplication::translate("PythonDocCore","Hide coordinate system?").toLatin1().data() ) = false ,
                               py::arg( QCoreApplication::translate("PythonDocCore","Number of samples per pixel").toLatin1().data() ) =1  );


  core.def("applicationSnapshot", &Core::applicationSnapshot, QCoreApplication::translate("PythonDocCore","Create a snapshot of the whole application").toLatin1().data() );

  core.def("applicationSnapshotName", &Core::applicationSnapshotName, QCoreApplication::translate("PythonDocCore","Set the baseName for the application snapshot").toLatin1().data() ,
                                                             py::arg( QCoreApplication::translate("PythonDocCore","BaseName for full application snapshots").toLatin1().data() ) );

  core.def("viewerSnapshot", static_cast<void (Core::*)()>(&Core::viewerSnapshot), QCoreApplication::translate("PythonDocCore","Take a snapshot from all viewers").toLatin1().data() );

  core.def("viewerSnapshot", static_cast<void (Core::*)(QString,bool,bool,bool,bool,int,int,bool,bool,int,bool)>(&Core::viewerSnapshot),
               QCoreApplication::translate("PythonDocCore","Create a snapshot with full control").toLatin1().data(),
               py::arg( QCoreApplication::translate("PythonDocCore","Filename of the snapshot").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Should the comments be written?").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Should only the comments of visible objects be written?").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Should only the comments of target objects be written?").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Store material info?").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Snapshot width").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Snapshot height").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Transparent background?").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Hide coordinate system?").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Multisampling count").toLatin1().data() ),
               py::arg( QCoreApplication::translate("PythonDocCore","Store the view in the metadata?").toLatin1().data() )  );


  core.def("resizeViewers", &Core::resizeViewers, QCoreApplication::translate("PythonDocCore","Resize the examinerViewer.").toLatin1().data() ,
                                                py::arg( QCoreApplication::translate("PythonDocCore","Width").toLatin1().data() ),
                                                py::arg( QCoreApplication::translate("PythonDocCore","Height").toLatin1().data() ) );

  core.def("resizeApplication", &Core::resizeApplication, QCoreApplication::translate("PythonDocCore","Resize the whole Application.").toLatin1().data() ,
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Width").toLatin1().data() ),
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Height").toLatin1().data() ) );

  core.def("writeVersionNumbers", &Core::writeVersionNumbers, QCoreApplication::translate("PythonDocCore","Write the current versions of all plugins to ini file").toLatin1().data() ,
                                                     py::arg( QCoreApplication::translate("PythonDocCore","Full path to a file where the versions should be written to.").toLatin1().data() )  );

//  core.def("objectList", &Core::objectList, QCoreApplication::translate("PythonDocCore","Return an id list of available object that has the given selection and type").toLatin1().data() ,
//                                   py::arg( QCoreApplication::translate("PythonDocCore","Either source or target").toLatin1().data()) ,
//                                   py::arg( QCoreApplication::translate("PythonDocCore","Object types as a stringlist").toLatin1().data() ));

//     /// return the list of available object that has the given selection and type
//     QList<int> objectList (QString _selection, QStringList _types);

  core.def("blockSceneGraphUpdates", &Core::blockSceneGraphUpdates, QCoreApplication::translate("PythonDocCore","Block the scenegraph updates for improved performance").toLatin1().data() );
  core.def("unblockSceneGraphUpdates", &Core::unblockSceneGraphUpdates, QCoreApplication::translate("PythonDocCore","Unblock the scenegraph updates").toLatin1().data() );


  core.def("setView", &Core::setView, QCoreApplication::translate("PythonDocCore","Set the encoded view for the primary viewport.").toLatin1().data() ,
                             py::arg( QCoreApplication::translate("PythonDocCore","The encoded view. (You can obtain one through \"Copy View\" in the context menu of the coordinates.)").toLatin1().data() )  );

  core.def("setViewAndWindowGeometry", &Core::setViewAndWindowGeometry, QCoreApplication::translate("PythonDocCore","Set the encoded view for the primary viewport and the full geometry of the application").toLatin1().data() ,
                                                               py::arg( QCoreApplication::translate("PythonDocCore","The encoded view. (You can obtain one through \"Copy View\" in the context menu of the coordinates.)").toLatin1().data() )  );

  core.def("addViewModeToolboxes", &Core::addViewModeToolboxes, QCoreApplication::translate("PythonDocCore","Set toolboxes for a viewmode  (This automatically adds the view mode if it does not exist)").toLatin1().data() ,
                                                      py::arg( QCoreApplication::translate("PythonDocCore","Name of the Viewmode").toLatin1().data() ),
                                                      py::arg( QCoreApplication::translate("PythonDocCore","';' separated list of toolboxes visible in this viewmode)").toLatin1().data() ));

  core.def("addViewModeToolbars", &Core::addViewModeToolbars, QCoreApplication::translate("PythonDocCore","Set toolbars for a viewmode  (This automatically adds the view mode if it does not exist)").toLatin1().data() ,
                                                     py::arg( QCoreApplication::translate("PythonDocCore","Name of the Viewmode").toLatin1().data() ),
                                                     py::arg( QCoreApplication::translate("PythonDocCore","';' separated list of toolbars visible in this viewmode)").toLatin1().data() ));

  core.def("addViewModeContextMenus", &Core::addViewModeContextMenus, QCoreApplication::translate("PythonDocCore","Set context Menus for a viewmode (This automatically adds the view mode if it does not exist)").toLatin1().data() ,
                                                             py::arg( QCoreApplication::translate("PythonDocCore","Name of the Viewmode").toLatin1().data() ),
                                                             py::arg( QCoreApplication::translate("PythonDocCore","';' separated list of Context Menus visible in this viewmode)").toLatin1().data() ));

  core.def("addViewModeIcon", &Core::addViewModeIcon, QCoreApplication::translate("PythonDocCore","Set Icon for a viewmode (This automatically adds the view mode if it does not exist)").toLatin1().data() ,
                                             py::arg( QCoreApplication::translate("PythonDocCore","Name of the Viewmode").toLatin1().data() ),
                                             py::arg( QCoreApplication::translate("PythonDocCore","filename of the icon (will be taken from OpenFlippers icon directory)").toLatin1().data() ));


  core.def("setToolBoxSide", &Core::setToolBoxSide, QCoreApplication::translate("PythonDocCore","Scripting function to set the side of the main window on which the toolbox should be displayed").toLatin1().data() ,
                                           py::arg( QCoreApplication::translate("PythonDocCore","The desired side of the toolboxes (either 'left' or 'right')").toLatin1().data() ) );

  core.def("setToolBoxActive", &Core::setToolBoxActive, QCoreApplication::translate("PythonDocCore","Activate or deaktivate a toolbox").toLatin1().data() ,
                                               py::arg( QCoreApplication::translate("PythonDocCore","Name of the toolbox.").toLatin1().data() ),
                                               py::arg( QCoreApplication::translate("PythonDocCore","Activate or deaktivate?").toLatin1().data() ));

  core.def("loadObject",  static_cast<int (Core::*)(QString)>(&Core::loadObject), QCoreApplication::translate("PythonDocCore","Load an object specified in file filename. This automatically determines which file plugin to use. It returns the id of the object in the scene or -1 on failure").toLatin1().data() ,
                                                                         py::arg( QCoreApplication::translate("PythonDocCore","Filename to load.").toLatin1().data() )  );

  core.def("startVideoCapture", &Core::startVideoCapture, QCoreApplication::translate("PythonDocCore","Start video capturing.").toLatin1().data() ,
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Basename for capturing").toLatin1().data() ),
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Frames per second").toLatin1().data() ),
                                                 py::arg( QCoreApplication::translate("PythonDocCore","Should the viewers be captured or the whole application?").toLatin1().data() ) );

  core.def("stopVideoCapture", &Core::stopVideoCapture, QCoreApplication::translate("PythonDocCore","Stop video capturing").toLatin1().data() );

  core.def("saveObject",  static_cast<bool (Core::*)(int,QString)>(&Core::saveObject), QCoreApplication::translate("PythonDocCore","Save object to file. If the file exists it will be overwritten.").toLatin1().data() ,
                                                                              py::arg( QCoreApplication::translate("PythonDocCore","Id of the object)").toLatin1().data() ),
                                                                              py::arg( QCoreApplication::translate("PythonDocCore","Complete path and filename").toLatin1().data() ));

  core.def("saveObjectTo",  static_cast<bool (Core::*)(int,QString)>(&Core::saveObjectTo), QCoreApplication::translate("PythonDocCore","Save object to file. The location can be chosen in a dialog. (GUI mode only!)").toLatin1().data() ,
                                                                               py::arg( QCoreApplication::translate("PythonDocCore","Id of the object)").toLatin1().data() ),
                                                                               py::arg( QCoreApplication::translate("PythonDocCore","Initial filename in the dialog").toLatin1().data() ));


//  bool saveObjectsTo( IdList _ids, QString _filename );


  core.def("saveAllObjects", &Core::saveAllObjects, QCoreApplication::translate("PythonDocCore","Saves all target objects. Exising files will be overriden. For new files, a dialog is shown (Only GUI Mode!)").toLatin1().data() );

  core.def("saveAllObjectsTo", &Core::saveAllObjectsTo, QCoreApplication::translate("PythonDocCore","Saves all target objects. The locations can be chosen in dialogs. (Only GUI Mode!)").toLatin1().data() );

  core.def("saveSettings",  static_cast<void (Core::*)()>(&Core::saveSettings), QCoreApplication::translate("PythonDocCore","Show the dialog to save settings. (only works if GUI is available)").toLatin1().data() );

  core.def("saveSettings",  static_cast<void (Core::*)(QString,bool,bool,bool,bool,bool,bool)>(&Core::saveSettings),
      QCoreApplication::translate("PythonDocCore","Save the current setting to the given file.").toLatin1().data(),
      py::arg( QCoreApplication::translate("PythonDocCore","Path of the file to save the settings to.").toLatin1().data() ),
      py::arg( QCoreApplication::translate("PythonDocCore","Save Object information into file?").toLatin1().data() ),
      py::arg( QCoreApplication::translate("PythonDocCore","Restrict to targeted objects?").toLatin1().data() ),
      py::arg( QCoreApplication::translate("PythonDocCore","Save objects into same path as settings file?").toLatin1().data() ),
      py::arg( QCoreApplication::translate("PythonDocCore","Prompt before overwriting files that already exist (gui mode only).").toLatin1().data() ),
      py::arg( QCoreApplication::translate("PythonDocCore","Save program settings?").toLatin1().data() ),
      py::arg( QCoreApplication::translate("PythonDocCore","Save plugin settings?").toLatin1().data() )  );

  core.def("loadObject",  static_cast<void (Core::*)()>(&Core::loadObject), QCoreApplication::translate("PythonDocCore","Show the dialog to load an object. (only works if GUI is available)").toLatin1().data() );

  core.def("loadSettings",  static_cast<void (Core::*)()>(&Core::loadSettings), QCoreApplication::translate("PythonDocCore","Show the dialog to load settings. (only works if GUI is available)").toLatin1().data() );

  core.def("loadSettings",  static_cast<void (Core::*)(QString)>(&Core::loadSettings), QCoreApplication::translate("PythonDocCore","Load settings from file.").toLatin1().data() ,
                                                                              py::arg( QCoreApplication::translate("PythonDocCore","Filename to load.").toLatin1().data() )  );

  core.def("getObjectId",  &Core::getObjectId, QCoreApplication::translate("PythonDocCore","Return identifier of object with specified name. Returns -1 if object was not found.").toLatin1().data() ,
                                      py::arg( QCoreApplication::translate("PythonDocCore","Filename or name of the object").toLatin1().data() )  );

  core.def("deserializeMaterialProperties",  &Core::deserializeMaterialProperties, QCoreApplication::translate("PythonDocCore","Deserialize the supplied material properties into the supplied object.").toLatin1().data() ,
                                                                          py::arg( QCoreApplication::translate("PythonDocCore","Id of the object").toLatin1().data() ),
                                                                          py::arg( QCoreApplication::translate("PythonDocCore","Material properties encoded as string").toLatin1().data() ) );

  core.def("serializeMaterialProperties",  &Core::serializeMaterialProperties, QCoreApplication::translate("PythonDocCore","Serialize and return the material properties of the supplied object.").toLatin1().data() ,
                                                                      py::arg( QCoreApplication::translate("PythonDocCore","Id of the object").toLatin1().data() ));

  core.def("activateToolbox", &Core::activateToolbox, QCoreApplication::translate("PythonDocCore","Expand or collapse a toolbox").toLatin1().data() ,
                                             py::arg( QCoreApplication::translate("PythonDocCore","Name of the plugin to which this toolbox gelongs").toLatin1().data() ),
                                             py::arg( QCoreApplication::translate("PythonDocCore","Name of the toolbox").toLatin1().data() ),
                                             py::arg( QCoreApplication::translate("PythonDocCore","Expand or collapse?").toLatin1().data() ) );

  core.def("saveOptions", &Core::saveOptions, QCoreApplication::translate("PythonDocCore","Save the current options to the standard ini file").toLatin1().data() );

  core.def("applyOptions", &Core::applyOptions, QCoreApplication::translate("PythonDocCore","After ini-files have been loaded and core is up or if options have been changed -> apply Options").toLatin1().data() );

  core.def("openIniFile", &Core::openIniFile, QCoreApplication::translate("PythonDocCore","Load information from an ini file").toLatin1().data() ,
                                              py::arg( QCoreApplication::translate("PythonDocCore","Name of the ini file").toLatin1().data() ),
                                              py::arg( QCoreApplication::translate("PythonDocCore","Load applications settings?").toLatin1().data() ),
                                              py::arg( QCoreApplication::translate("PythonDocCore","Load plugin settings?").toLatin1().data() ),
                                              py::arg( QCoreApplication::translate("PythonDocCore"," Load objects defined in the ini file?").toLatin1().data() ));


//  /** \brief Create an script object from a ui file
//   *
//   * This function will load an ui file, set up a qwidget from that and makes it available
//   * under _objectName for scripting.
//   * @param _objectName The name in scripting environment used for the new object
//   * @param _uiFilename ui file to load
//   */
//  void createWidget(QString _objectName, QString _uiFilename, bool _show = true);

  core.def("setViewMode", &Core::setViewMode, QCoreApplication::translate("PythonDocCore","Switch to the given viewmode").toLatin1().data() ,
                                     py::arg( QCoreApplication::translate("PythonDocCore","Name of the viewmode to enable").toLatin1().data() )  );


  core.def("getCurrentViewMode", &Core::getCurrentViewMode, QCoreApplication::translate("PythonDocCore","Get the name of the current viewMode").toLatin1().data() );

  core.def("setViewModeIcon", &Core::setViewModeIcon, QCoreApplication::translate("PythonDocCore","Set an icon for a view Mode").toLatin1().data() ,
                                               py::arg( QCoreApplication::translate("PythonDocCore","Name of the mode for the icon").toLatin1().data() ),
                                               py::arg( QCoreApplication::translate("PythonDocCore","Filename of the icon").toLatin1().data() )  );

  core.def("moveToolBoxToTop", &Core::moveToolBoxToTop, QCoreApplication::translate("PythonDocCore","Move selected toolbox to the top of side area").toLatin1().data() ,
                                               py::arg( QCoreApplication::translate("PythonDocCore","Name of the Toolbox to be moved").toLatin1().data() )  );

  core.def("moveToolBoxToBottom", &Core::moveToolBoxToBottom, QCoreApplication::translate("PythonDocCore","Move selected toolbox to the bottom of side area").toLatin1().data() ,
                                                     py::arg( QCoreApplication::translate("PythonDocCore","Name of the Toolbox to be moved").toLatin1().data() )  );

  core.def("showReducedMenuBar", &Core::showReducedMenuBar, QCoreApplication::translate("PythonDocCore","Show only a reduced menubar").toLatin1().data() ,
                                                   py::arg( QCoreApplication::translate("PythonDocCore","Reduced Menubar?").toLatin1().data() )  );

  core.def("executePythonScriptFile", &Core::executePythonScriptFile, QCoreApplication::translate("PythonDocCore","Open the given file and execute its contents as a python script").toLatin1().data() ,
                                                             py::arg( QCoreApplication::translate("PythonDocCore","Filename of the script").toLatin1().data() )  );


  core.def("executePythonScript", &Core::executePythonScript, QCoreApplication::translate("PythonDocCore","Execute a text as a python script").toLatin1().data() ,
                                                     py::arg( QCoreApplication::translate("PythonDocCore","The text of the script").toLatin1().data() )  );

  core.def("exitApplication", &Core::exitApplication, QCoreApplication::translate("PythonDocCore","Exit the application").toLatin1().data() );
  core.def("exitFailure", &Core::exitFailure, QCoreApplication::translate("PythonDocCore","Use this function in unit tests, if you detected a failure. Therefore the test functions will recognize that something went wrong.").toLatin1().data() );
  core.def("finishSplash", &Core::finishSplash, QCoreApplication::translate("PythonDocCore","Hide the splash screen").toLatin1().data() );

  core.def("objectUpdated", &Core::slotObjectUpdated, QCoreApplication::translate("PythonDocCore","Tell the core that an object has been updated").toLatin1().data(),
                                             py::arg( QCoreApplication::translate("PythonDocCore","ID of the updated object").toLatin1().data() ),
                                             py::arg( QCoreApplication::translate("PythonDocCore","What has been updated? String list separated by ; . Possible update types include: All,Visibility,Geometry,Topology,Selection,VertexSelection,EdgeSelection,HalfedgeSelection,FaceSelection,KnotSelection,Color,Texture,State  ").toLatin1().data() ) = UPDATE_ALL );


//    //load slots
//    emit setSlotDescription("createWidget(QString,QString)", tr("Create a widget from an ui file"),
//                            QString(tr("Object name,ui file")).split(","),
//                            QString(tr("Name of the new widget in script,ui file to load")).split(","));

//    emit setSlotDescription("objectList(QString,QStringList)", tr("Returns object list"),
//                            QString(tr("Selection type,Object types")).split(","),




}

