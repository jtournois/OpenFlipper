/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/include/pybind11/pybind11.h>
#include <pybind11/include/pybind11/numpy.h>
#include <OpenFlipper/common/UpdateType.hh>
#include <QString>

namespace py = pybind11;

namespace pybind11 { namespace detail {
    template <> struct type_caster<QString> {
    public:
        /**
         * This macro establishes the name 'str' in
         * function signatures and declares a local variable
         * 'value' of type QVariant
         */
        PYBIND11_TYPE_CASTER(QString, _("str"));

        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a QString
         * instance or return false upon failure. The second argument
         * indicates whether implicit conversions should be applied.
         */
        bool load(handle src, bool ) {

            /* Extract PyObject from handle */
            PyObject *source = src.ptr();

            if (!PyUnicode_Check(source))
              return false;

            Py_ssize_t size;
            const char *ptr = PyUnicode_AsUTF8AndSize(source, &size);

            if (!ptr) {
                return false;
            }

            /* Now try to convert into a C++ int */
            value = QString::fromUtf8(ptr, size);

            /* Ensure return code was OK (to avoid out-of-range errors etc) */
            return ( !PyErr_Occurred() );
        }

        /**
         * Conversion part 2 (C++ -> Python): convert an QString instance into
         * a Python object. The second and third arguments are used to
         * indicate the return value policy and parent object (for
         * ``return_value_policy::reference_internal``) and are generally
         * ignored by implicit casters.
         */
        static handle cast(QString src, return_value_policy /* policy */, handle /* parent */) {
            return (PyUnicode_FromString( src.toUtf8().data()) );
        }
    };
}} // namespace pybind11::detail

/** \page python_scripting_vector_type Vector data type used for python scripting
 *
 *   The matrix type Vector is mapped to python to handle Vector operations.
 *
 *   The integrated conversion can map the C++ Vector type to and from a tuple, a list or a numpy array.
 *   The preferred choice is the numpy array. Tuple and list have to contain 3 elements. A numpy array
 *   has to have a dimension of 1, with 3 elements.
 *
 *   Creating a matrix in python is the done like in the following example:
 *   \code
 *     import numpy as np
 *
 *     vector = np.array([1.0,0.0,0.0]);
 *   \endcode
 *
 *   The conversion from C++ to python will always create a numpy array on the python side.
 */
namespace pybind11 { namespace detail {
    template <> struct type_caster<Vector> {
    public:
        /**
         * This macro establishes the name 'str' in
         * function signatures and declares a local variable
         * 'value' of type QVariant
         */
        PYBIND11_TYPE_CASTER(Vector, _("Vector"));

        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a Vector
         * instance or return false upon failure. The second argument
         * indicates whether implicit conversions should be applied.
         */
        bool load(handle src, bool convert ) {

            /* Extract PyObject from handle */
             PyObject* source = src.ptr();

             if (  PyList_Check(source) ) {

               if ( PyList_Size(source) != 3) {
                 PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: List size should be 3!");
                 throw py::error_already_set();
                 return false;
               }

               /* Now try to convert into a C++ int */
               value = Vector(PyFloat_AsDouble(PyList_GetItem(source,0)),PyFloat_AsDouble(PyList_GetItem(source,1)),PyFloat_AsDouble(PyList_GetItem(source,2)));

             } else if ( PyTuple_Check(source) ) {
               if ( PyTuple_Size(source) != 3) {
                 PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Tuple size should be 3!");
                 throw py::error_already_set();
                 return false;
               }

               /* Now try to convert into a C++ int */
               value = Vector(PyFloat_AsDouble(PyTuple_GetItem(source,0)),PyFloat_AsDouble(PyTuple_GetItem(source,1)),PyFloat_AsDouble(PyTuple_GetItem(source,2)));

             } else if ( py::cast<py::array>(source) ) {

               py::array array = py::cast<py::array>(source);

               if (  array.size() != 3) {
                 PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Numpy Array size should be 3!");
                 throw py::error_already_set();
                 return false;
               }

               if (!convert && !py::array_t<double>::check_(src)) {
                 PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Numpy Array, wrong dtype, conversion disabled");
                 return false;
               }

               auto buf = py::array_t<double, py::array::c_style | py::array::forcecast>::ensure(src);

               if (!buf) {
                 PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Numpy Array, conversion failed.");
                 return false;
               }
               if (buf.ndim() != 1 || buf.size() != 3) {
                 PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Numpy Array dimension or size error. Dimension should be one, size 3!");
                 return false;
               }

               value = Vector(buf.data());
               return true;

             } else {
               PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Not a list or a tuple or a numpy array.");
               throw py::error_already_set();
               return false;
             }

             /* Ensure return code was OK (to avoid out-of-range errors etc) */
             return ( !PyErr_Occurred() );
        }

        /**
         * Conversion part 2 (C++ -> Python): convert an Vector instance into
         * a Python object. The second and third arguments are used to
         * indicate the return value policy and parent object (for
         * ``return_value_policy::reference_internal``) and are generally
         * ignored by implicit casters.
         */
        static handle cast(Vector src, return_value_policy /* policy */, handle parent ) {
          // Create numpy array
          py::array a(3, src.data());
          return a.release();
        }
    };
}} // namespace pybind11::detail

/** \page python_scripting_matrix4x4_type Matrix4x4 data type used for python scripting
 *
 *   The matrix type Matrix4x4 is mapped to python to handle matrix operations.
 *
 *   The integrated conversion can map the C++ Matrix4x4 type to and from a tuple, a list or a numpy array.
 *   The preferred choice is the numpy array. Tuple and list have to contain 16 elements. A numpy array
 *   has to have a dimension of 2, with 4x4 elements. All data is assumed to be given column major. Therefore
 *   the first for elements define the first column and so on.
 *
 *   Creating a matrix in python is the done like in the following example:
 *   \code
 *     import numpy as np
 *
 *     matr = np.array( [[1.0,0.0,0.0,0.0],[0.0,1.0,0.0,0.0],[0.0,0.0,1.0,0.0],[0.0,0.0,0.0,1.0]])
 *   \endcode
 *
 *   The conversion from C++ to python will always create a numpy array on the python side.
 */
namespace pybind11 { namespace detail {
    template <> struct type_caster<Matrix4x4> {
    public:
        /**
         * This macro establishes the name 'str' in
         * function signatures and declares a local variable
         * 'value' of type Matrix4x4
         */
        PYBIND11_TYPE_CASTER(Matrix4x4, _("Matrix4x4"));

        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a Matrix4x4
         * instance or return false upon failure. The second argument
         * indicates whether implicit conversions should be applied.
         */
        bool load(handle src, bool convert ) {

            /* Extract PyObject from handle */
             PyObject* source = src.ptr();

             if (  PyList_Check(source) ) {

               if ( PyList_Size(source) != 16) {
                 PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: List size should be 16!");
                 throw py::error_already_set();
                 return false;
               }

               double convert[16] =  { PyFloat_AsDouble(PyList_GetItem(source,0)),
                                       PyFloat_AsDouble(PyList_GetItem(source,1)),
                                       PyFloat_AsDouble(PyList_GetItem(source,2)),
                                       PyFloat_AsDouble(PyList_GetItem(source,3)),
                                       PyFloat_AsDouble(PyList_GetItem(source,4)),
                                       PyFloat_AsDouble(PyList_GetItem(source,5)),
                                       PyFloat_AsDouble(PyList_GetItem(source,6)),
                                       PyFloat_AsDouble(PyList_GetItem(source,7)),
                                       PyFloat_AsDouble(PyList_GetItem(source,8)),
                                       PyFloat_AsDouble(PyList_GetItem(source,9)),
                                       PyFloat_AsDouble(PyList_GetItem(source,0)),
                                       PyFloat_AsDouble(PyList_GetItem(source,11)),
                                       PyFloat_AsDouble(PyList_GetItem(source,12)),
                                       PyFloat_AsDouble(PyList_GetItem(source,13)),
                                       PyFloat_AsDouble(PyList_GetItem(source,14)),
                                       PyFloat_AsDouble(PyList_GetItem(source,15))};

               /* Now convert into a C++ Matrix4x4 */
               value = Matrix4x4(convert);

             } else if ( PyTuple_Check(source) ) {
               if ( PyTuple_Size(source) != 16) {
                 PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: Tuple size should be 3!");
                 throw py::error_already_set();
                 return false;
               }

               double convert[16] =  { PyFloat_AsDouble(PyTuple_GetItem(source,0)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,1)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,2)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,3)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,4)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,5)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,6)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,7)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,8)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,9)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,10)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,11)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,12)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,13)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,14)),
                                       PyFloat_AsDouble(PyTuple_GetItem(source,15))};

               /* Now convert into a C++ Matrix4x4 */
               value = Matrix4x4(convert);

             } else if ( py::cast<py::array>(source) ) {

               py::array array = py::cast<py::array>(source);

               if (  array.size() != 16) {
                 PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: Numpy Array size should be 16!");
                 throw py::error_already_set();
                 return false;
               }

               if (!convert && !py::array_t<double>::check_(src)) {
                 PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: Numpy Array, wrong dtype, conversion disabled");
                 return false;
               }

               auto buf = py::array_t<double, py::array::c_style | py::array::forcecast>::ensure(src);

               if (!buf) {
                 PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: Numpy Array, conversion failed.");
                 return false;
               }

               // Check Dimension   : Matrix has 2 dimensions
               // Check buffer size : 4x4 -> 16 entries
               // Check shape       : 2 dimensions, each has size 4
               if (buf.ndim() != 2 || buf.size() != 16 || buf.shape()[0] !=4  || buf.shape()[1] !=4) {
                 PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: Numpy Array dimension or size error. Dimension should be four, size 16, and shape 4x4!");
                 return false;
               }

               value = Matrix4x4(buf.data());
               return true;

             } else {
               PyErr_SetString(PyExc_RuntimeError, "Matrix4x4 Conversion: Not a list or a tuple or a numpy array.");
               throw py::error_already_set();
               return false;
             }

             /* Ensure return code was OK (to avoid out-of-range errors etc) */
             return ( !PyErr_Occurred() );
        }

        /**
         * Conversion part 2 (C++ -> Python): convert an Matrix4x4 instance into
         * a Python object. The second and third arguments are used to
         * indicate the return value policy and parent object (for
         * ``return_value_policy::reference_internal``) and are generally
         * ignored by implicit casters.
         */
        static handle cast(Matrix4x4 src, return_value_policy /* policy */, handle parent ) {
          // Create numpy array
          py::array a({4,4}, src.data());
          return a.release();
        }
    };
}} // namespace pybind11::detail


/** \page python_scripting_IDList_type IDList data type used for python scripting
 *
 *   The list type IdList is mapped to python.
 *
 *   The integrated conversion can map the C++ IdList type to and from a tuple, a list or a numpy array.
 *   The preferred choice is the numpy array.
 *
 *   Creating a list in python is the done like in the following example:
 *   \code
 *     import numpy as np
 *
 *     list = np.array( [3,16,18])
 *   \endcode
 *
 *   The conversion from C++ to python will always create a numpy array on the python side.
 */
namespace pybind11 { namespace detail {
    template <> struct type_caster<IdList> {
    public:
        /**
         * This macro establishes the name 'IdList' in
         * function signatures and declares a local variable
         * 'value' of type IdList
         */
        PYBIND11_TYPE_CASTER(IdList, _("IdList"));

        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a IdList
         * instance or return false upon failure. The second argument
         * indicates whether implicit conversions should be applied.
         */
        bool load(handle src, bool convert ) {

            /* Extract PyObject from handle */
             PyObject* source = src.ptr();

             if (  PyList_Check(source) ) {

               const auto size = PyList_Size(source);

               value.resize(size);

               for ( auto i = 0 ; i < size ; ++i ) {
                 value[i] = static_cast<int>(PyLong_AsLong(PyList_GET_ITEM(source,i)));
               }

             } else if ( PyTuple_Check(source) ) {

               const auto size = PyTuple_Size(source);

               value.resize(size);

               for ( auto i = 0 ; i < size ; ++i ) {
                 value[i] = static_cast<int>(PyLong_AsLong(PyTuple_GET_ITEM(source,i)));
               }

             } else if ( py::cast<py::array>(source) ) {

               py::array array = py::cast<py::array>(source);

               if (  array.ndim() != 1) {
                 PyErr_SetString(PyExc_RuntimeError, "IdList Conversion: Numpy Array dimension should be one!");
                 throw py::error_already_set();
                 return false;
               }

               auto buf = py::array_t<int, py::array::c_style | py::array::forcecast>::ensure(src);

               if (!buf) {
                 PyErr_SetString(PyExc_RuntimeError, "IdList Conversion: Numpy Array, conversion failed.");
                 return false;
               }

               std::cerr << "Buffer size : " << buf.size() << std::endl;

               value.resize(buf.size());
               for ( auto i = 0 ; i < buf.size() ; ++i ) {
                 value[i] = *buf.data(i) ;
               }

               return true;

             } else {
               PyErr_SetString(PyExc_RuntimeError, "Vector Conversion: Not a list or a tuple or a numpy array.");
               throw py::error_already_set();
               return false;
             }

             /* Ensure return code was OK (to avoid out-of-range errors etc) */
             return ( !PyErr_Occurred() );
        }

        /**
         * Conversion part 2 (C++ -> Python): convert an IdList instance into
         * a Python object. The second and third arguments are used to
         * indicate the return value policy and parent object (for
         * ``return_value_policy::reference_internal``) and are generally
         * ignored by implicit casters.
         */
        static handle cast(IdList src, return_value_policy /* policy */, handle parent ) {
          // Create numpy array
          py::array a(src.size(), src.data());
          return a.release();
        }
    };
}} // namespace pybind11::detail

/** \page python_scripting_UpdateType_type UpdateType data type used for python scripting
 *
 *   The list type UpdateType is mapped to python.
 *
 *   The integrated conversion can map the C++ UpdateType type to and from a String.
 *   The string has to be a list separated by ';' if it should be a combination of update types.
 *
 *   \code
*      core.objectUpdated(5,"Selection;Geometry")
 *   \endcode
 *
 *   Update types implemented in Core are:
 *   - All
 *   - Visibility
 *   - Geometry
 *   - Topology
 *   - Selection
 *   - VertexSelection
 *   - EdgeSelection
 *   - HalfedgeSelection
 *   - FaceSelection
 *   - KnotSelection
 *   - Color
 *   - Texture
 *   - State
 *
 *   Other plugins might have implemented additional update types, which will be automatically mapped.
 */
namespace pybind11 { namespace detail {
    template <> struct type_caster<UpdateType> {
    public:
        /**
         * This macro establishes the name 'UpdateType' in
         * function signatures and declares a local variable
         * 'value' of type UpdateType
         */
        PYBIND11_TYPE_CASTER(UpdateType, _("UpdateType"));

        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a UpdateType
         * instance or return false upon failure. The second argument
         * indicates whether implicit conversions should be applied.
         */
        bool load(handle src, bool ) {

            /* Extract PyObject from handle */
            PyObject *source = src.ptr();

            if (!PyUnicode_Check(source))
              return false;

            Py_ssize_t size;
            const char *ptr = PyUnicode_AsUTF8AndSize(source, &size);

            if (!ptr) {
                return false;
            }

            QString updateString = QString::fromUtf8(ptr, size);

            if ( updateString.contains("UPDATE_ALL") ) {
              std::cerr << "Update_ALL" << std::endl;
              value = UPDATE_ALL;

              return true;
            }


            if ( updateString.contains("All") ) {
              std::cerr << "Update_ALL" << std::endl;
              value = UPDATE_ALL;

              return true;
            }

            UpdateType type;

            QStringList updateList = updateString.split(";");

            for ( auto i = 0 ; i < updateList.size() ; ++i ) {
              type |= updateType(updateList[i]);
              std::cerr << "Update " <<  updateList[i].toStdString() << std::endl;
            }

            value = type;

            /* Ensure return code was OK (to avoid out-of-range errors etc) */
            return ( !PyErr_Occurred() );
        }

        /**
         * Conversion part 2 (C++ -> Python): convert an UpdateType instance into
         * a Python object. The second and third arguments are used to
         * indicate the return value policy and parent object (for
         * ``return_value_policy::reference_internal``) and are generally
         * ignored by implicit casters.
         */
        static handle cast(UpdateType src, return_value_policy /* policy */, handle /* parent */) {
            return (PyUnicode_FromString( updateTypeName(src).toUtf8().data()) );
        }
    };
}} // namespace pybind11::detail



/** \page python_scripting_DaraType_type DataType data type used for python scripting
 *
 *   The DataType type describing objects in OpenFlipper is mapped to python.
 *
 *   The integrated conversion can map the C++ DataType type to and from a String. *
 *   \code
*      core.objectUpdated(5,"Selection;Geometry")
 *   \endcode
 *
 *   Some examples for possible data types are:
 *   - All
 *   - Group
 *   - BSplineCurve
 *   - BSplineSurface
 *   - Coordsys
 *   - Light
 *   - TriangleMesh
 *   - PolyMesh
 *   - HexahedralMesh
 *   - PolyhedralMesh
 *   - TetrahedralMesh
 *   - Plane
 *   - PolyLine
 *   - PolyLineCollection
 *   - QtWidget
 *   - Skeleton
 *   - Sphere
 *   - SplatCloud
 *
 *   Other plugins might have implemented additional data types, which will be automatically mapped.
 */
namespace pybind11 { namespace detail {
    template <> struct type_caster<DataType> {
    public:
        /**
         * This macro establishes the name 'DataType' in
         * function signatures and declares a local variable
         * 'value' of type DataType
         */
        PYBIND11_TYPE_CASTER(DataType, _("DataType"));

        /**
         * Conversion part 1 (Python->C++): convert a PyObject into a DataType
         * instance or return false upon failure. The second argument
         * indicates whether implicit conversions should be applied.
         */
        bool load(handle src, bool ) {

            /* Extract PyObject from handle */
            PyObject *source = src.ptr();

            if (!PyUnicode_Check(source))
              return false;

            Py_ssize_t size;
            const char *ptr = PyUnicode_AsUTF8AndSize(source, &size);

            if (!ptr) {
                return false;
            }

            QString typeString = QString::fromUtf8(ptr, size);

            value = typeId(typeString);

            /* Ensure return code was OK (to avoid out-of-range errors etc) */
            return ( !PyErr_Occurred() );
        }

        /**
         * Conversion part 2 (C++ -> Python): convert an DataType instance into
         * a Python object. The second and third arguments are used to
         * indicate the return value policy and parent object (for
         * ``return_value_policy::reference_internal``) and are generally
         * ignored by implicit casters.
         */
        static handle cast(DataType src, return_value_policy /* policy */, handle /* parent */) {
            return (PyUnicode_FromString( typeName(src).toUtf8().data()) );
        }
    };
}} // namespace pybind11::detail



