/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#pragma once

#include <OpenFlipper/Core/Core.hh>

/** \brief This class provides OpenFlippers Python interpreter
 *
 */
class PythonInterpreter : public QObject {
  Q_OBJECT

  signals:
     /// Log with OUT,WARN or ERR as type
     void log(Logtype _type , QString _message );

public:


     ~PythonInterpreter();

     /** \brief Creates or returns an instance of the interpreter
      *
      * There will only be one instance of the PythonInterpreter to ensure
      * that we only run one context.
      *
      * @return
      */
     static PythonInterpreter* getInstance();

     /** \brief Run a script. Output is passed to the standard logging facilities of OpenFlipper
      *
      * @param _script
      * @return
      */
     bool runScript(QString _script);

     /** \ brief run a script and return the output as a QString
      *
      * @param _script
      * @return
      */
     QString runScriptOutput(QString _script);


private:

     /** \brief private constructor because of singleton
      *
      */
     PythonInterpreter();

     /** \brief Initialize OpenFlipper Python Interpreter
      *
      */
     void initPython();

     /** \brief Resets the interpreter and all states
      *
      */
     void resetInterpreter();


     bool modulesInitialized();

     /** \brief Callback to redirect cout log to OpenFlipper logger
      *
      * @param w Log string
      */
     void pyOutput(const char* w);

     /** \brief Callback to redirect cerr log to OpenFlipper logger
      *
      * @param w Log string
      */
     void pyError(const char* w);


private:
     bool externalLogging_;
     QString LogErr;
     QString LogOut;
};

/** \brief give Core pointer to PythonInterpreter for exporting of Core functionality to python
 *
 * This function is called by OpenFlipper.cc to pass a pointer of the OpenFlipper Core to
 * the python side. This allows the PythonInterpreter class to export functions from the core
 * and call them directly.
 *
 * @param _core Pointer to OpenFlipper application core
 */
void setCorePointer( Core* _core );



