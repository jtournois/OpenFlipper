/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


//=============================================================================
//
//  MyTypes
//
//=============================================================================


//== INCLUDES =================================================================

#include "BaseObjectCore.hh"
#include "Types.hh"
#include <OpenFlipper/BasePlugin/PluginFunctionsCore.hh>
#include <OpenFlipper/BasePlugin/PluginFunctions.hh>

#include <QJsonDocument>
#include <QJsonObject>


//== TYPEDEFS =================================================================

//== Variables =================================================================

//== CLASS DEFINITION =========================================================

/** This counter is used to provide unique object ids. Every time a object is created
 * the counter is incremented and a new objectId given to the object. DONT TOUCH IT.
 */
static int idGenerator = 1;

/** Static member for no object initialization
 *
 */
int BaseObject::NOOBJECT = -1;

BaseObject::BaseObject(const BaseObject& _object) :
  QObject(),
  id_(idGenerator),
  persistentId_(_object.persistentId_),
  objectType_(_object.objectType_),
  flags_(_object.flags_),
  visible_(_object.visible_),
  parentItem_(0),
  path_("."),
  filename_(""),
  commentsByKey_(_object.commentsByKey_)

{
  // Increase id generator as we created a new object
  ++idGenerator;

  // Generate a usable name based on the copied object
  name_ = "Copy of " + _object.name_;

  childItems_.clear();
  dataMap_.clear();
  
  // Iterate over all per Object datas and try to copy them
  QMap< QString, PerObjectData* >::const_iterator mapIter = _object.dataMap_.begin();  
  while ( mapIter != _object.dataMap_.end() ) {
    // Try to get a copy of the object.
    PerObjectData* copiedData = mapIter.value()->copyPerObjectData();
    
    if ( copiedData ) {
      dataMap_.insert(mapIter.key(),copiedData);
    } else {
      std::cerr << "Failed to copy per Object Data: " << mapIter.key().toStdString() << std::endl;
    }
    
    ++mapIter;
  }
  
  // If the pointer is 0 then we are creating the object root
  // and we do not have anything to attach this object to
  // otherwise, we attach the new object to the object root
  if ( PluginFunctions::objectRoot() ) {

    setParent(PluginFunctions::objectRoot());
    PluginFunctions::objectRoot()->appendChild(this);

    // New object
    PluginFunctions::increaseObjectCount();

    // If the new one is target, we also have to track this
    if ( target() )
      PluginFunctions::increaseTargetCount();
  }
  
  // Add object to object container
  PluginFunctions::addObjectToMap( id(), this );
  
  getObjectManager()->objectCreated(id());
}

BaseObject::BaseObject(BaseObject* _parent) :
  QObject() ,
  id_(-1),
  persistentId_(-1),
  objectType_(DATA_UNKNOWN),
  flags_(),
  visible_(true),
  parentItem_(_parent),
  name_("NONAME")
{
  id_ = idGenerator;
  ++ idGenerator;
  
  // If the pointer is 0 then something went wrong or we are the root node
  if ( _parent ) {
    
    _parent->appendChild(this);
    PluginFunctions::increaseObjectCount();
    
  } else {
    
    if ( PluginFunctions::objectRoot() ) {
      setParent(PluginFunctions::objectRoot());
      PluginFunctions::objectRoot()->appendChild(this);
      PluginFunctions::increaseObjectCount();
    }
    
  }
  
  // Add object to object container
  PluginFunctions::addObjectToMap( id(), this );
  
  getObjectManager()->objectCreated(id());
}


BaseObject::~BaseObject() {

  deleteData();
  
  PluginFunctions::decreaseObjectCount();
  
  if ( target() )
    PluginFunctions::decreaseTargetCount();
  
  getObjectManager()->objectDeleted(id());

}


// ===============================================================================
// Object Identification
// ===============================================================================

int BaseObject::id() const {
  return id_;
}

int BaseObject::persistentId() const {
  return persistentId_;
}

void BaseObject::persistentId( int _id ) {
  persistentId_ = _id;
}


// ===============================================================================
// Data
// ===============================================================================

void BaseObject::cleanup() {
  persistentId_ = -1;
  path_       = ".";
  flags_.clear();

  visible_    = true;
  name_       = "NONAME";
  filename_ = "";
}

// ===============================================================================
// Data Type Handling
// ===============================================================================

bool BaseObject::dataType(DataType _type) const {
  if ( _type == DATA_ALL ) {
    return true;
  }

  return ( objectType_ & _type);
}

DataType BaseObject::dataType() const {
  return BaseObject::objectType_;
}

void BaseObject::setDataType(DataType _type) {
  if ( objectType_ != DATA_UNKNOWN )
    std::cerr << "BaseObect : overwriting data type" << std::endl;
  objectType_ = _type;
}

// ===============================================================================
// Object Information
// ===============================================================================


QString BaseObject::getObjectinfo() {
  QString output;

  output += "Info for Object with id " + QString::number(id()) +"\n";
  output += "Object is : ";
  if ( target() )
    output += "target ";
  if ( source() )
    output += " source";

  if ( visible() )
    output += " visible";
  else
    output += " invisible";

  output +="\n";

  return output;
}

void BaseObject::printObjectInfo() {
  std::cout << getObjectinfo().toStdString();
}


// ===============================================================================
// flag Handling
// ===============================================================================

bool BaseObject::target() {
  return flag("target");
}

void BaseObject::target(bool _target) {

  if ( target() != _target ) {
    
    if ( _target )
      PluginFunctions::increaseTargetCount();
    else
      PluginFunctions::decreaseTargetCount();
  }
  
  setFlag("target", _target);
  
}

bool BaseObject::source() {
  return flag("source");
}

void BaseObject::source(bool _source) {
  setFlag("source", _source);
}

bool BaseObject::flag(QString _flag)
{
  return flags_.contains(_flag);
}

void BaseObject::setFlag(QString _flag, bool _set)
{
  bool emitted = false;
  
  if (flags_.contains(_flag))
  {
    if (!_set) {
      flags_.removeAll(_flag);
      emit objectSelectionChanged(id());
      emitted = true;
    }
  }
  else
  {
    if (_set) {
      flags_ << _flag;
      emit objectSelectionChanged(id());
      emitted = true;
    }
  }
  
  //always emit if its a group
  if ( !emitted && isGroup() )
    emit objectSelectionChanged(id());
}

QStringList BaseObject::flags()
{
  return flags_;
}

// ===============================================================================
// Object visualization
// ===============================================================================

bool BaseObject::visible() {
  return visible_;
}

void BaseObject::visible(bool _visible) {
  // Only do something if this is really a change
  if (  visible_ != _visible ) {
    visible_ = _visible;
    
    emit visibilityChanged( id() );
    
  } else {
    
    //always emit if its a group
    if ( isGroup() )
      emit visibilityChanged( id() );
  }
}

// ===============================================================================
// ===============================================================================
// Tree Structure :
// ===============================================================================
// ===============================================================================

BaseObject* BaseObject::last() {
  //indexOf

//   // Visit child item of this node
//   if ( childItems_.size() > 0 ) {
//      return childItems_[0];
//   }
//
//   // No Child Item so visit the next child of the parentItem_
//   if ( parentItem_ ) {
//
//     BaseObject* parentPointer = parentItem_;
//     BaseObject* thisPointer   = this;
//
//     // while we are not at the root node
//     while ( parentPointer ) {
//
//       // If there is an unvisited child of the parent, return this one
//       if ( parentPointer->childCount() > ( thisPointer->row() + 1) ) {
//         return parentPointer->childItems_[ thisPointer->row() + 1 ];
//       }
//
//       // Go to the next level
//       thisPointer   = parentPointer;
//       parentPointer = parentPointer->parentItem_;
//
//     }
//
//     return thisPointer;
//   }
//
//   return this;
  std::cerr << "Last not implemented yet! " << std::endl;
  return 0;

}

// ===============================================================================
// ===============================================================================

BaseObject* BaseObject::next() {
  // Visit child item of this node
  if ( childItems_.size() > 0 ) {
     return childItems_[0];
  }

  // No Child Item so visit the next child of the parentItem_
  if ( parentItem_ ) {

    BaseObject* parentPointer = parentItem_;
    BaseObject* thisPointer   = this;

    // while we are not at the root node
    while ( parentPointer ) {

      // If there is an unvisited child of the parent, return this one
      int nextIndex = ( thisPointer->row() + 1);
      if ( parentPointer->childCount() > nextIndex ) {
        return parentPointer->childItems_[ nextIndex ];
      }

      // Go to the next level
      thisPointer   = parentPointer;
      parentPointer = parentPointer->parentItem_;

    }

    return thisPointer;
  }

  return this;

}

// ===============================================================================
// ===============================================================================

int BaseObject::level() {
  int level = 0;
  BaseObject* current = this;

  // Go up and count the levels to the root node
  while ( current->parent() != 0 ) {
    level++;
    current = current->parent();
  }

  return level;
}

// ===============================================================================
// Parent
// ===============================================================================

int BaseObject::row() const
{
    if (parentItem_)
        return parentItem_->childItems_.indexOf(const_cast<BaseObject*>(this));

    return 0;
}

BaseObject* BaseObject::parent()
{
  return parentItem_;
}

const BaseObject* BaseObject::parent() const
{
  return parentItem_;
}

/// Set the parent pointer
void BaseObject::setParent(BaseObject* _parent) {
  // remove this child from the old parents list
  if ( parentItem_ != 0 ) {
    parentItem_->removeChild(this);
   
    if ( !_parent->childItems_.contains(this) )
      _parent->appendChild(this);
  }
  
  // Store new parent
  parentItem_ = _parent;
  
  // Tell other plugins about this change
  emit objectPropertiesChanged(id());
}


// ===============================================================================
// Children
// ===============================================================================

void BaseObject::appendChild(BaseObject *item)
{
  if ( !childItems_.contains(item) )
    childItems_.append(item);
  else 
    std::cerr << "Warning! Trying to append a child twice! Remove the append calls from your File plugin!" << std::endl;
}

BaseObject *BaseObject::child(int row)
{
    return childItems_.value(row);
}

int BaseObject::childCount() const
{
    return childItems_.count();
}

BaseObject* BaseObject::childExists(int _objectId) {

  // Check if this object has the requested id
  if ( id_ == _objectId )
    return this;

  // search in children
  for ( int i = 0 ; i < childItems_.size(); ++i ) {
    BaseObject* tmp = childItems_[i]->childExists(_objectId);
    if ( tmp != 0)
      return tmp;
  }

  return 0;
}

BaseObject* BaseObject::childExists(QString _name) {

  // Check if this object has the requested id
  if ( name() == _name )
    return this;

  // search in children
  for ( int i = 0 ; i < childItems_.size(); ++i ) {
    BaseObject* tmp = childItems_[i]->childExists(_name);
    if ( tmp != 0)
      return tmp;
  }

  return 0;
}

void BaseObject::removeChild( BaseObject* _item ) {

  bool found = false;
  QList<BaseObject*>::iterator i;
  for (i = childItems_.begin(); i != childItems_.end(); ++i) {
     if ( *i == _item ) {
        found = true;
        break;
     }
  }

  if ( !found ) {
    std::cerr << "Illegal remove request" << std::endl;
    return;
  }

  childItems_.erase(i);
}

QList< BaseObject* > BaseObject::getLeafs() {

  QList< BaseObject* > items;

  for ( int i = 0 ; i < childItems_.size(); ++i ) {
    items = items + childItems_[i]->getLeafs();
  }

  // If we are a leave...
  if ( childCount() == 0 )
    items.push_back(this);

  return items;
}

void BaseObject::deleteSubtree() {

  // call function for all children of this node
  for ( int i = 0 ; i < childItems_.size(); ++i) {

    // remove the subtree recursively
    childItems_[i]->deleteSubtree();

    // delete child
    delete childItems_[i];
  }

  // clear the array
  childItems_.clear();
}

// ===============================================================================
// Grouping
// ===============================================================================
int BaseObject::group() const {
  // Skip root node
  if ( parent() == 0 )
    return -1;

  // Dont count root node as a group
  if ( parent()->parent() == 0 )
    return -1;

  // Only consider groups
  if ( !parent()->dataType(DATA_GROUP) )
    return -1;

  // Get the group id
  return ( parent()->id() );

}

bool BaseObject::isGroup() const {
//   return ( (childItems_.size() > 0) || dataType(DATA_GROUP) ) ;
  return ( dataType(DATA_GROUP) ) ;
};


bool BaseObject::isInGroup( int _id ) const {
  const BaseObject* current = this;

  // Go up and check for the group id
  do {

    // Check if we found the id
    if ( current->id() == _id )
      return true;

    // Move on to parent object
    current = current->parent();
  } while ( current != 0 );

  return false;
}

bool BaseObject::isInGroup( QString _name ) const {
  const BaseObject* current = this;

  // Go up and check for the group name
  do {

    // Check if this object is a group and if it has the serach name
    if ( current->dataType( DATA_GROUP ) && (current->name() == _name) )
      return true;

    // Move on to parent object
    current = current->parent();

  } while ( current != 0 );

  return false;
}

std::vector< int > BaseObject::getGroupIds() {
  std::vector< int > groups;

  BaseObject* current = this;

  // Go up and collect all groups in the given order
  do {

    // collect only groups
    if ( current->dataType( DATA_GROUP ) )
      // Skip the root Object
      if ( current->parent() != 0 )
        groups.push_back( current->id() );

    // Move on to parent object
    current = current->parent();
  } while ( current != 0 );

  return groups;
}

QStringList BaseObject::getGroupNames() {
  QStringList groups;

  BaseObject* current = this;

  // Go up and collect all groups in the given order
  do {

    // collect only groups
    if ( current->dataType( DATA_GROUP ) )
      // Skip the root Object
      if ( current->parent() != 0 )
        groups.push_back( current->name() );

    // Move on to parent object
    current = current->parent();
  } while ( current != 0 );

  return groups;
}

// ===============================================================================
// Name and path Handling
// ===============================================================================

QString BaseObject::filename() const
{
  return filename_;
}

void BaseObject::setFileName(const QString &_filename)
{
  filename_ = _filename;
}

void BaseObject::setFromFileName(const QString &_filename ) {
  QFileInfo file_info(_filename);
  setPath(file_info.path());
  QString filename = file_info.fileName();
  setFileName(filename);
}

void BaseObject::setName(QString _name ) {
  name_ = _name;

  // Tell plugins about the name change
  emit objectPropertiesChanged(id());
}

QString BaseObject::name() const {
  return name_;
}

QString BaseObject::path() const {
  return path_;
}

void BaseObject::setPath(const QString &_path ) {
  path_ = _path;
}

// ===============================================================================
// Content
// ===============================================================================
void BaseObject::update(UpdateType /*_type*/) {
}

void BaseObject::dumpTree() {

  // Add spaces to visualize level
  for ( int i = 0 ; i < level() ; ++i  )
    std::cerr << "   ";

  std::cerr << "Node ";
  std::cerr << std::string(name().toLatin1());

  std::cerr << " with id : ";
  std::cerr << id();

  // Write the type of this Object
  std::cerr << " and type " << typeName(dataType()).toStdString()  <<  std::endl;

  // call function for all children of this node
  for ( int i = 0 ; i < childItems_.size(); ++i)
    childItems_[i]->dumpTree();

}

BaseObject* BaseObject::copy() {
  std::cerr << "Copy not supported by this Object" << std::endl;
  return  0;
}


// ===============================================================================
// per Object data functions
// ===============================================================================

void
BaseObject::
setObjectData( QString _dataName , PerObjectData* _data ) {
  dataMap_.insert( _dataName, _data );
}

void
BaseObject::
clearObjectData( QString _dataName ) {
  if (dataMap_.contains(_dataName))
    dataMap_.remove(_dataName);
}


bool
BaseObject::
hasObjectData( QString _dataName )
{
  return dataMap_.contains(_dataName);
}


PerObjectData*
BaseObject::
objectData( QString _dataName ) {
  if (dataMap_.contains(_dataName))
    return dataMap_.value(_dataName);
  else
    return 0;
}

void
BaseObject::
deleteData() {

  QMapIterator<QString, PerObjectData* > i(dataMap_);
  while (i.hasNext()) {
      i.next();
      delete i.value();
  }

  dataMap_.clear();

}

QString& BaseObject::getCommentByKey(const QString &key) {
     return commentsByKey_[key];
 }


 const QString BaseObject::getCommentByKey(const QString &key) const {
     return commentsByKey_.value(key);
 }


 bool BaseObject::hasCommentForKey(const QString &key) const {
     return commentsByKey_.contains(key);
 }


 bool BaseObject::hasComments() const {
     return !commentsByKey_.empty();
 }

 void BaseObject::clearComment(const QString &key) {
     commentsByKey_.remove(key);
 }

 void BaseObject::clearAllComments() {
     commentsByKey_.clear();
 }


 const QMap<QString, QString>& BaseObject::getAllComments() const {
     return commentsByKey_;
 }

const QString BaseObject::getAllCommentsFlat() const {
    QStringList result;

    result.append(QString("BEGIN Comments for object \"%1\"").arg(name()));

    /*
     * Compose JSON parsable object.
     */
    QJsonObject comment_obj;
    for (QMap<QString, QString>::const_iterator it = commentsByKey_.begin(), it_end = commentsByKey_.end();
            it != it_end; ++it) {

        QJsonParseError json_error;
        QString test_json_str = QString::fromUtf8("{\"test\": %1}").arg(it.value());
        QByteArray test_json_ba = test_json_str.toUtf8();
        QJsonDocument test_json = QJsonDocument::fromJson(test_json_ba, &json_error);
        if (json_error.error != QJsonParseError::NoError) {
            comment_obj[it.key()] = it.value();
        } else {
            comment_obj[it.key()] = test_json.object().value("test");
        }
    }
    result.append(QString::fromUtf8(QJsonDocument(comment_obj).toJson(QJsonDocument::Indented)));

    result.append(QString("END Comments for object \"%1\"\n").arg(name()));

    return result.join("\n");
}


QMap<QString, PerObjectData*>& BaseObject::getPerObjectDataMap() {
  return dataMap_;
}


//=============================================================================
