#include "PythonFunctions.hh"
#include "PythonFunctionsCore.hh"

#include <map>
#include <iostream>
#include <QStringList>

std::map<QString,QObject*> map_;


void setPluginPointer(QString _name , QObject* _pointer) {
  map_[_name] = _pointer;
}

QObject* getPluginPointer(QString _name) {

  auto it = map_.find(_name);
  if (  it != map_.end() ) {
    return it->second;
  } else {
    std::cerr << "No plugin found with name " << _name.toStdString() << " for PythonFunctions" << std::endl;
    return nullptr;
  }
}

const QStringList getPythonPlugins() {
  QStringList plugins;

  for ( auto it =  map_.begin() ; it != map_.end() ; ++it ) {
    plugins.push_back(it->first);
  }

  return plugins;
}
