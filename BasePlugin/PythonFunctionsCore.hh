#include <OpenFlipper/common/GlobalDefines.hh>

#include <QObject>
#include <QString>

DLLEXPORT
void setPluginPointer(QString _name , QObject* _pointer);

DLLEXPORT
const QStringList getPythonPlugins();
