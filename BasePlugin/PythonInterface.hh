/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#pragma once


/** \file PythonInterface.hh
*
* Interface for enabling Python Interfacing for a plugin. \ref pythonInterfacePage
*/


 /** \brief Interface class for exporting functions to python
 *
 * \n
 * \ref pythonInterfacePage "Detailed description"
 * \n
 *
 */
class PythonInterface {

  public :

    /// Destructor
    virtual ~PythonInterface() {};

};

/** \page pythonInterfacePage Python Interface
\n
\n
  The Python interface enables OpenFlipper to export plugin functions to the integrated python interpreter of OpenFlipper.
  Only a few steps are required to export functions in a plugin.

  1. Tell cmake, that your plugin supports the Python Interface. In your CMakeLists.txt add the Keyword PYTHONINTERFACE. E.g. :
  \code
   include (plugin)
   openflipper_plugin (PYTHONINTERFACE )
  \endcode

  2. Add the corresponding includes and calls to your plugin header:
    <ul>
      <li> Add #include <OpenFlipper/BasePlugin/PythonInterface.hh> in your plugins header file
      <li> derive your plugin from the class PythonInterface
      <li> add Q_INTERFACES(PythonInterface) to your plugin class
    </ul>

  3. Add a subdirectory called PythonInterface to your plugin directory
  4. In this directory create a file called Python.cc with contents similar to the following Code:
  \code

   #include <pybind11/include/pybind11/pybind11.h>
   #include <pybind11/include/pybind11/embed.h>

   #include <YourPlugin.hh>
   #include <OpenFlipper/BasePlugin/PythonFunctions.hh>

   namespace py = pybind11;

   PYBIND11_EMBEDDED_MODULE(YourPlugin, m) {  // yourPlugin will will be the module name in Python ( ... import yourPlugin )

     QObject* pluginPointer = getPluginPointer("Your");  // This will retrieve your plugin instance pointer. The name is the plugin name set via the name() function.

     // Export our plugin. Make sure that the c++ worlds core object is not deleted if
     // the python side gets deleted!!
     py::class_< YourPlugin,std::unique_ptr<YourPlugin, py::nodelete> > plugin(m, "Your");  // This will provide you with the existing instance of your plugin in Python.

     // On the c++ side we will just return the existing core instance
     // and prevent the system to recreate a new core as we need
     // to work on the existing one.
     plugin.def(py::init([&pluginPointer]() { return qobject_cast<YourPlugin*>(pluginPointer); }));  // This will provide you with the existing instance of your plugin in Python.

     plugin.def("viewAll",   static_cast<void (YourPlugin::*)()>(&YourPlugin::viewAll),"Change View in all viewers to view whole scene");  // Example export of an overloaded member function
     plugin.def("viewAll",   static_cast<void (ViewControlPlugin::*)(int)>(&YourPlugin::viewAll),"Change View in given viewer to view whole scene",py::arg("Id of the viewer which should be switched") );  // Example export of an overloaded member function

     plugin.def("orthographicProjection",   static_cast<void (ViewControlPlugin::*)()>(&ViewControlPlugin::orthographicProjection) ); // simple export example
   }

  \endcode

*/

Q_DECLARE_INTERFACE(PythonInterface,"OpenFlipper.PythonInterface/1.0")

