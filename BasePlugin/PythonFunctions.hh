
#include <OpenFlipper/common/GlobalDefines.hh>

#include <QObject>
#include <QString>

/** \brief Get the pointer to a plugin instance to register Python functions
 *
 * @param _name
 * @return
 */
DLLEXPORT
QObject* getPluginPointer(QString _name);

