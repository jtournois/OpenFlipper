# http://www.andres.sc/graph.html
# We call it andresgraph to avoid ambiguity

# - Try to find ANDRESGRAPH
# Once done this will define
#  ANDRESGRAPH_FOUND         - System has ANDRESGRAPH
#  ANDRESGRAPH_INCLUDE_DIRS  - The ANDRESGRAPH include directories

if (ANDRESGRAPH_INCLUDE_DIR)
  # in cache already
  set(ANDRESGRAPH_FOUND TRUE)
  set(ANDRESGRAPH_INCLUDE_DIRS "${ANDRESGRAPH_INCLUDE_DIR}" )
else (ANDRESGRAPH_INCLUDE_DIR)

find_path( ANDRESGRAPH_INCLUDE_DIR 
	   NAMES andres/graph/multicut-lifted/kernighan-lin.hxx
           PATHS $ENV{ANDRESGRAPH_DIR}
                 /usr/include/
                 /usr/local/include/
                 /opt/local/include/
          )

set(ANDRESGRAPH_INCLUDE_DIRS "${ANDRESGRAPH_INCLUDE_DIR}" )


include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBCPLEX_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(ANDRESGRAPH  DEFAULT_MSG
                                  ANDRESGRAPH_INCLUDE_DIR)

mark_as_advanced(ANDRESGRAPH_INCLUDE_DIR)

endif(ANDRESGRAPH_INCLUDE_DIR)
