include (ACGCommon)

set ( ADDITIONAL_PLUGINLIB_LIBS "")
set ( ADDITIONAL_PLUGINLIB_INCS "")
set ( ADDITIONAL_PLUGINLIB_COMPILER_FLAGS "")
set ( ADDITIONAL_PLUGINLIB_LINK_DIRS "")

# search all ObjectTypes in the ObjectType directory for additional build information
file (
  GLOB _plugin_buildinfos
  RELATIVE "${CMAKE_SOURCE_DIR}"
  "${CMAKE_SOURCE_DIR}/ObjectTypes/*/CMakeLists.txt"
  "${CMAKE_SOURCE_DIR}/Package*/ObjectTypes/*/CMakeLists.txt"

)

# include all cmake files found for objecttypes here
foreach ( _buildInfo ${_plugin_buildinfos})
  include ("${CMAKE_SOURCE_DIR}/${_buildInfo}")
endforeach ()
  
# Find all packages and include their Paths here like the toplevel would be
file(GLOB OF_PACKAGES RELATIVE "${CMAKE_SOURCE_DIR}" "${CMAKE_SOURCE_DIR}/Package*" )

foreach ( OF_PACKAGE ${OF_PACKAGES})
   list(APPEND PACKAGE_INCLUDES "${CMAKE_SOURCE_DIR}/${OF_PACKAGES}")
endforeach ()


# search all lib directories in packages for OpenFlipper
file (
    GLOB PACKAGE_DIRS
      "${CMAKE_SOURCE_DIR}/Package-*"
)

include_directories (
  ..
  ${PACKAGE_DIRS}
  ${ACG_INCLUDE_DIR}
  ${OPENMESH_INCLUDE_DIRS}
  ${CMAKE_SOURCE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}
  ${OPENGL_INCLUDE_DIR}
  ${ADDITIONAL_PLUGINLIB_INCS}
  ${PACKAGE_INCLUDES}
)

link_directories (
  ${ADDITIONAL_PLUGINLIB_LINK_DIRS}
)

#=======================================================
# General defines
#=======================================================

# generate dllexport macros on windows
if (WIN32)
  add_definitions (
    -DPLUGINLIBDLL
    -DACGDLL
    -DUSEACG
  )
endif ()

# define core version
add_definitions (
    -DCORE_VERSION="${OPENFLIPPER_VERSION}"
)

# Mark this build part as building OpenFLippers Core
add_definitions(-DOPENFLIPPERPLUGINLIB )

#=======================================================
# Add possibility to debug OpenFlipperSettings
#=======================================================

if ( NOT DEFINED OPENFLIPPER_SETTINGS_DEBUG )
  set( OPENFLIPPER_SETTINGS_DEBUG false CACHE BOOL "Enable or disable debugging of OpenFlipperSettings" )
  mark_as_advanced(OPENFLIPPER_SETTINGS_DEBUG)
endif()

if ( OPENFLIPPER_SETTINGS_DEBUG )
 add_definitions(-DOPENFLIPPER_SETTINGS_DEBUG )
endif()

#=======================================================
# Collect files to compile
#=======================================================

# source code directories
set (directories
  ../BasePlugin
  ../ACGHelper
  ../common
  ../threads
  ../common/bsp
  ../INIFile
  ../widgets/glWidget
  ../publicWidgets/objectSelectionWidget
  ../widgets/filePicker 
  ../Utils/FileIO
  ../Utils/Memory
)

# collect all header,source and ui files
acg_append_files (headers  "*.hh" ${directories})
acg_append_files (sources  "*.cc" ${directories})
acg_append_files (ui-files "*.ui" ${directories})


source_group(TREE ${CMAKE_SOURCE_DIR} FILES ${sources} ${headers} ${ui-files} ) 

#=======================================================
# Cleanup
#=======================================================

# remove template cc files from source file list
acg_drop_templates (sources)

#=======================================================
# Create library
#=======================================================

if (QT5_FOUND)
  acg_add_library (OpenFlipperPluginLib SHARED ${sources} ${headers} )
endif ()


if( ADDITIONAL_PLUGINLIB_COMPILER_FLAGS )
  set_target_properties (
    OpenFlipperPluginLib PROPERTIES
    COMPILE_FLAGS ${ADDITIONAL_PLUGINLIB_COMPILER_FLAGS}
  )
endif()


target_link_libraries (OpenFlipperPluginLib
  ${OPENMESH_LIBRARIES}
  ACG
  ${QT_LIBRARIES}
  ${OPENGL_LIBRARIES}
  ${FTGL_LIBS}
  ${ADDITIONAL_PLUGINLIB_LIBS}
)

#=======================================================
# Setup translation files for OpenFlipperPluginLib
#=======================================================

set (trans_files ${sources})
list (APPEND trans_files ${ui})
list (APPEND trans_files ${headers})
acg_add_translations (OpenFlipperPluginLib "de_DE" "${trans_files}")




