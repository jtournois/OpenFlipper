/*===========================================================================*\
 *                                                                           *
 *                              OpenFlipper                                  *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/





//=============================================================================
//
//  CLASS QtBaseViewer - IMPLEMENTATION
//
//=============================================================================


//== INCLUDES =================================================================

#include <ACG/GL/acg_glew.hh>
#include "QtBaseViewer.hh"
#include <QStatusBar>
#include <QToolButton>

#include "../GL/GLState.hh"

#include "mono.xpm"
#include "stereo.xpm"

#define monoIcon         mono_xpm
#define stereoIcon       stereo_xpm

//== NAMESPACES ===============================================================

namespace ACG {
namespace QtWidgets {


//== IMPLEMENTATION ==========================================================

void QtBaseViewer::setEyeDistance(double _distance) {
  eyeDist_ = _distance;
  updateGL();
}

double QtBaseViewer::eyeDistance( ) {
  return eyeDist_;
}

void QtBaseViewer::setFocalDistance(double _distance) {
  focalDist_ = _distance;
  updateGL();
}

double QtBaseViewer::focalDistance( ) {
  return focalDist_;
}

void QtBaseViewer::toggleStereoMode() {
  QtBaseViewer::setStereoMode(!stereo_);
}

//-----------------------------------------------------------------------------


void
QtBaseViewer::setStereoMode(bool _b)
{
  stereo_ = _b;

  if (stereo_)
  {
    statusbar_->showMessage("Stereo enabled");
    stereoButton_->setIcon( QPixmap(stereoIcon) );
  }
  else
  {
    statusbar_->showMessage("Stereo disabled");
    stereoButton_->setIcon( QPixmap(monoIcon) );
    makeCurrent();
    ACG::GLState::drawBuffer(GL_BACK);
  }

  updateGL();
}

//=============================================================================
} // namespace QtWidgets
} // namespace ACG
//=============================================================================
