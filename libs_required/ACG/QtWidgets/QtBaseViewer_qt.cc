/*===========================================================================*\
 *                                                                           *
 *                              OpenFlipper                                  *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/





//=============================================================================
//
//  CLASS QtBaseViewer - IMPLEMENTATION
//
//=============================================================================


//== INCLUDES =================================================================

#include "QtBaseViewer.hh"
#include "QtGLGraphicsScene.hh"
#include "QtGLGraphicsView.hh"
//#include "QtSceneGraphWidget.hh"
#include "QtWheel.hh"

#include <QMimeData>
#include <QToolButton>

#include <QClipboard>
#include <QApplication>
#include <QPushButton>
#include <QStatusBar>
#include <QColorDialog>
#include <QFileDialog>
#include <QTimer>

#include <QGLFormat>
#include <QGLWidget>

#include <QDesktopWidget>
#include <QButtonGroup>

#include <QGraphicsWidget>
#include <QGraphicsGridLayout>
#include <QGraphicsProxyWidget>

#include "move.xpm"
#include "light.xpm"
#include "info.xpm"
#include "home.xpm"
#include "set_home.xpm"
#include "viewall.xpm"
#include "pick.xpm"
#include "persp.xpm"
#include "ortho.xpm"
#include "scenegraph.xpm"
#include "mono.xpm"


#define homeIcon         home_xpm
#define sethomeIcon      set_home_xpm
#define moveIcon         move_xpm
#define lightIcon        light_xpm
#define questionIcon     info_xpm
#define viewallIcon      viewall_xpm
#define pickIcon         pick_xpm
#define perspectiveIcon  persp_xpm
#define orthoIcon        ortho_xpm
#define sceneGraphIcon   scenegraph_xpm
#define monoIcon         mono_xpm


#ifdef max
#  undef max
#endif

#ifdef min
#  undef min
#endif


//== NAMESPACES ===============================================================

namespace ACG {
namespace QtWidgets {


//== IMPLEMENTATION ==========================================================

static const char          VIEW_MAGIC[] =
  "ACG::QtWidgets::QGLViewerWidget encoded view";

//== IMPLEMENTATION ==========================================================


void QtBaseViewer::makeCurrent() {
  glWidget_->makeCurrent();
}

void QtBaseViewer::swapBuffers() {
  glWidget_->swapBuffers();
}



void
QtBaseViewer::copyToImage( QImage& _image,
			   unsigned int /* _l */ , unsigned int /* _t */ ,
			   unsigned int /* _w */ , unsigned int /* _h */ ,
			   GLenum /* _buffer */ )
{
  makeCurrent();
  _image = glWidget_->grabFrameBuffer(true);
}


void
QtBaseViewer::createWidgets(const QGLFormat* _format,
                            QStatusBar* _sb,
                            const QtBaseViewer* _share)
{
  statusbar_=privateStatusBar_=0;
  setStatusBar(_sb);
  drawMenu_=0;
  funcMenu_=0;
  pickMenu_=0;


  // contains splitter and eventually status bar
  // QT3:  Q3VBoxLayout* layout=new Q3VBoxLayout(this,0,0,"toplevel layout");
  QVBoxLayout* layout=new QVBoxLayout(this);
  layout->setSpacing( 0 );
  layout->setMargin( 0 );

  // contains glarea and buttons

  // QT3:  Q3Frame* work=new Q3Frame(this,"box-glarea-buttons");
  QFrame* work=new QFrame(this);

  layout->addWidget(work,1); // gets all stretch


  // private status bar
  assert(statusbar_!=0);
  if (privateStatusBar_!=0)
    layout->addWidget(privateStatusBar_,0); // no stretch


  // Construct GL context & widget
  QGLWidget* share = 0;
  if (_share)  share = _share->glWidget_;

  QGLFormat format;
  format.setAlpha(true);
  if (_format!=0) format = *_format;

  glWidget_ = new QGLWidget(format, 0, share);
  glView_ = new QtGLGraphicsView(this, work);
  glScene_ = new QtGLGraphicsScene (this);

  glView_->setViewport(glWidget_);
  glView_->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
  glView_->setScene(glScene_);
  glView_->setFrameStyle(QFrame::NoFrame);

  wheelZ_=new QtWheel( 0,"wheel-z",QtWheel::Vertical);
  wheelZ_->setMinimumSize(wheelZ_->sizeHint());
  wheelZ_->setMaximumSize(wheelZ_->sizeHint());
  connect(wheelZ_,SIGNAL(angleChangedBy(double)),
	  this,SLOT(slotWheelZ(double)));
  wheelZ_->setToolTip( "Translate along <b>z-axis</b>.");
  wheelZ_->setWhatsThis( "Translate along <b>z-axis</b>.");
  if ((options_&ShowWheelZ)==0)
    wheelZ_->hide();

  wheelY_=new QtWheel( 0,"wheel-y",QtWheel::Horizontal);
  wheelY_->setMinimumSize(wheelY_->sizeHint());
  wheelY_->setMaximumSize(wheelY_->sizeHint());
  connect(wheelY_,SIGNAL(angleChangedBy(double)),
          this,SLOT(slotWheelY(double)));
  wheelY_->setToolTip("Rotate around <b>y-axis</b>.");
  wheelY_->setWhatsThis( "Rotate around <b>y-axis</b>.");
  if ((options_&ShowWheelY)==0)
    wheelY_->hide();

  wheelX_=new QtWheel( 0,"wheel-x",QtWheel::Vertical);
  wheelX_->setMinimumSize(wheelX_->sizeHint());
  wheelX_->setMaximumSize(wheelX_->sizeHint());
  connect(wheelX_,SIGNAL(angleChangedBy(double)),
          this,SLOT(slotWheelX(double)));
  wheelX_->setToolTip("Rotate around <b>x-axis</b>.");
  wheelX_->setWhatsThis( "Rotate around <b>x-axis</b>.");
  if ((options_&ShowWheelX)==0)
    wheelX_->hide();


  QGraphicsWidget *wheelX = glScene_->addWidget (wheelX_);
  QGraphicsWidget *wheelY = glScene_->addWidget (wheelY_);
  QGraphicsWidget *wheelZ = glScene_->addWidget (wheelZ_);

  wheelX_->setWindowOpacity (0.5);
  wheelY_->setWindowOpacity (0.5);
  wheelZ_->setWindowOpacity (0.5);

  wheelX->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
  wheelY->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
  wheelZ->setCacheMode(QGraphicsItem::DeviceCoordinateCache);

  glBaseLayout_ = new QGraphicsGridLayout;
  glBaseLayout_->addItem(wheelX, 1, 0);
  glBaseLayout_->addItem(wheelY, 2, 1);
  glBaseLayout_->addItem(wheelZ, 1, 3);

  glBaseLayout_->setColumnStretchFactor(0,0);
  glBaseLayout_->setColumnStretchFactor(1,0);
  glBaseLayout_->setColumnStretchFactor(2,1);
  glBaseLayout_->setColumnStretchFactor(3,0);

  glBaseLayout_->setRowStretchFactor(0,1);
  glBaseLayout_->setRowStretchFactor(1,0);
  glBaseLayout_->setRowStretchFactor(2,0);

  glBase_ = new QGraphicsWidget;
  glBase_->setLayout(glBaseLayout_);
  glScene_->addItem(glBase_);
  glBase_->setGeometry (glScene_->sceneRect ());
  //QRectF r = glScene_->sceneRect ();

  connect ( glScene_, SIGNAL( sceneRectChanged( const QRectF & ) ),
            this, SLOT( sceneRectChanged( const QRectF & ) ) );

  // If popupEnabled_ this signal will not be emitted!
  // If popupEnabled_ is set to false the Contextmenu Mode will be set to customContextMenuRequested
  // and this signal will be emitted on right click
  connect( glView_ , SIGNAL( customContextMenuRequested( const QPoint& ) ) ,
           this    , SIGNAL( signalCustomContextMenuRequested( const QPoint& ) ) );

  // is stereo possible ?
  if (format.stereo())
    std::cerr << "Stereo buffer requested: "
	      << (glWidget_->format().stereo() ? "ok\n" : "failed\n");


  // toolbar
  buttonBar_= new QToolBar( "Viewer Toolbar", work );
  buttonBar_->setOrientation(Qt::Vertical);

  moveButton_ = new QToolButton( buttonBar_ );
  moveButton_->setIcon( QPixmap(moveIcon) );
  moveButton_->setMinimumSize( 16, 16 );
  moveButton_->setMaximumSize( 32, 32 );
  moveButton_->setToolTip( "Switch to <b>move</b> mode." );
  moveButton_->setWhatsThis(
                  "Switch to <b>move</b> mode.<br>"
                  "<ul><li><b>Rotate</b> using <b>left</b> mouse button.</li>"
                  "<li><b>Translate</b> using <b>middle</b> mouse button.</li>"
                  "<li><b>Zoom</b> using <b>left+middle</b> mouse buttons.</li></ul>" );
  QObject::connect( moveButton_, SIGNAL( clicked() ),
                    this,        SLOT( examineMode() ) );

  buttonBar_->addWidget( moveButton_)->setText("Move");

  lightButton_ = new QToolButton( buttonBar_ );
  lightButton_->setIcon( QPixmap(lightIcon) );
  lightButton_->setMinimumSize( 16, 16 );
  lightButton_->setMaximumSize( 32, 32 );
  lightButton_->setToolTip("Switch to <b>light</b> mode.");
  lightButton_->setWhatsThis(
                  "Switch to <b>light</b> mode.<br>"
                  "Rotate lights using left mouse button.");
  QObject::connect( lightButton_, SIGNAL( clicked() ),
                    this,         SLOT( lightMode() ) );
  buttonBar_->addWidget( lightButton_)->setText("Light");


  pickButton_ = new QToolButton( buttonBar_ );
  pickButton_->setIcon( QPixmap(pickIcon) );
  pickButton_->setMinimumSize( 16, 16 );
  pickButton_->setMaximumSize( 32, 32 );
  pickButton_->setToolTip("Switch to <b>picking</b> mode.");
  pickButton_->setWhatsThis(
                  "Switch to <b>picking</b> mode.<br>"
                  "Use picking functions like flipping edges.<br>"
                  "To change the mode use the right click<br>"
                  "context menu in the viewer.");
  QObject::connect( pickButton_, SIGNAL( clicked() ),
                    this,        SLOT( pickingMode() ) );
  buttonBar_->addWidget( pickButton_)->setText("Pick");


  questionButton_ = new QToolButton( buttonBar_ );
  questionButton_->setIcon( QPixmap(questionIcon) );
  questionButton_->setMinimumSize( 16, 16 );
  questionButton_->setMaximumSize( 32, 32 );
  questionButton_->setToolTip("Switch to <b>identification</b> mode.");
  questionButton_->setWhatsThis(
                  "Switch to <b>identification</b> mode.<br>"
                  "Use identification mode to get information "
                  "about objects. Click on an object and see "
                  "the log output for information about the "
                  "object.");
  QObject::connect( questionButton_, SIGNAL( clicked() ),
                    this,            SLOT( questionMode() ) );
  buttonBar_->addWidget( questionButton_)->setText("Question");

  buttonBar_->addSeparator();

  homeButton_ = new QToolButton( buttonBar_ );
  homeButton_->setIcon( QPixmap(homeIcon) );
  homeButton_->setMinimumSize( 16, 16 );
  homeButton_->setMaximumSize( 32, 32 );
  homeButton_->setCheckable( false );
  homeButton_->setToolTip("Restore <b>home</b> view.");
  homeButton_->setWhatsThis(
                  "Restore home view<br><br>"
                  "Resets the view to the home view");
  QObject::connect( homeButton_, SIGNAL( clicked() ),
                    this,        SLOT( home() ) );
  buttonBar_->addWidget( homeButton_)->setText("Home");


  setHomeButton_ = new QToolButton( buttonBar_ );
  setHomeButton_->setIcon( QPixmap(sethomeIcon) );
  setHomeButton_->setMinimumSize( 16, 16 );
  setHomeButton_->setMaximumSize( 32, 32 );
  setHomeButton_->setCheckable( false );
  setHomeButton_->setToolTip("Set <b>home</b> view");
  setHomeButton_->setWhatsThis(
                  "Store home view<br><br>"
                  "Stores the current view as the home view");
  QObject::connect( setHomeButton_, SIGNAL( clicked() ),
                    this,           SLOT( setHome() ) );
  buttonBar_->addWidget( setHomeButton_)->setText("Set Home");


  viewAllButton_ = new QToolButton( buttonBar_ );
  viewAllButton_->setIcon( QPixmap(viewallIcon) );
  viewAllButton_->setMinimumSize( 16, 16 );
  viewAllButton_->setMaximumSize( 32, 32 );
  viewAllButton_->setCheckable( false );
  viewAllButton_->setToolTip("View all.");
  viewAllButton_->setWhatsThis(
                  "View all<br><br>"
                  "Move the objects in the scene so that"
                  " the whole scene is visible.");
  QObject::connect( viewAllButton_, SIGNAL( clicked() ),
                    this,           SLOT( viewAll() ) );
  buttonBar_->addWidget( viewAllButton_)->setText("View all");


  projectionButton_ = new QToolButton( buttonBar_ );
  projectionButton_->setIcon( QPixmap(perspectiveIcon) );
  projectionButton_->setMinimumSize( 16, 16 );
  projectionButton_->setMaximumSize( 32, 32 );
  projectionButton_->setCheckable( false );
  projectionButton_->setToolTip(
                "Switch between <b>perspective</b> and "
                "<b>parrallel</b> projection mode.");
  projectionButton_->setWhatsThis(
                "Switch projection modes<br><br>"
                "Switch between <b>perspective</b> and "
                "<b>parrallel</b> projection mode.");
  QObject::connect( projectionButton_, SIGNAL( clicked() ),
                    this,              SLOT( toggleProjectionMode() ) );
  buttonBar_->addWidget( projectionButton_)->setText( "Projection" );


  if (glWidget_->format().stereo())
  {
    stereoButton_ = new QToolButton( buttonBar_ );
    stereoButton_->setIcon( QPixmap(monoIcon) );
    stereoButton_->setMinimumSize( 16, 16 );
    stereoButton_->setMaximumSize( 32, 32 );
    stereoButton_->setCheckable( true );
    stereoButton_->setToolTip( "Toggle stereo viewing");
    stereoButton_->setWhatsThis(
                  "Toggle stereo mode<br><br>"
                  "Use this button to switch between stereo "
                  "and mono view. To use this feature you need "
                  "a stereo capable graphics card and a stereo "
                  "display/projection system.");
    QObject::connect( stereoButton_, SIGNAL( clicked() ),
                      this,          SLOT( toggleStereoMode() ) );
    buttonBar_->addWidget( stereoButton_)->setText( "Stereo");
  }

  buttonBar_->addSeparator();

  sceneGraphButton_ = new QToolButton( buttonBar_ );
  sceneGraphButton_->setIcon( QPixmap(sceneGraphIcon) );
  sceneGraphButton_->setMinimumSize( 16, 16 );
  sceneGraphButton_->setMaximumSize( 32, 32 );
  sceneGraphButton_->setCheckable( false );
  sceneGraphButton_->setToolTip("Toggle scene graph viewer.");
  sceneGraphButton_->setWhatsThis(
                  "Toggle scene graph viewer<br><br>"
                  "The scene graph viewer enables you to examine the "
                  "displayed scene graph and to modify certain nodes.<br><br>"
                  "There are three modi for the scene graph viewer:"
                  "<ul><li><b>hidden</b></li>"
                  "<li><b>split</b>: share space</li>"
                  "<li><b>dialog</b>: own dialog window</li></ul>"
                  "This button toggles between these modi.");
  QObject::connect( sceneGraphButton_, SIGNAL( clicked() ),
                    this,              SLOT( showSceneGraphDialog() ) );
  buttonBar_->addWidget( sceneGraphButton_)->setText( "SceneGraph" );

  glLayout_ = new QGridLayout(work);
  glLayout_->setSpacing( 0 );
  glLayout_->setMargin( 0 );

  glLayout_->addWidget(glView_,    0,0);
  glLayout_->addWidget(buttonBar_, 0,1);

  glLayout_->setColumnStretch(0,1);
  glLayout_->setColumnStretch(1,0);

  if (!(options_ & ShowToolBar))
    buttonBar_->hide();

}

bool QtBaseViewer::hasOpenGL()
{
  return QGLFormat::hasOpenGL();
}

//=============================================================================
} // namespace QtWidgets
} // namespace ACG
//=============================================================================
