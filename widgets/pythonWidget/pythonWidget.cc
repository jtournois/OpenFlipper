/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/


// If OpenFlipper builds with python support, we can integrate the interpreter.
// Otherwise we build only a dummy widget with no interpreter backend.
#ifdef PYTHON_ENABLED
  #include <pybind11/include/pybind11/pybind11.h>
  #include <pybind11/include/pybind11/embed.h>
  #include <OpenFlipper/PythonInterpreter/PythonInterpreter.hh>
#endif

#include "pythonWidget.hh"
#include "PythonSyntaxHighlighter.hh"
#include <OpenFlipper/common/GlobalOptions.hh>
#include <OpenFlipper/BasePlugin/PythonFunctionsCore.hh>
#include <iostream>
#include <QObject>


PythonWidget::PythonWidget(QWidget *parent )
    : QMainWindow(parent)
{
  setupUi(this);

  PythonSyntaxHighlighter *pythonHighlighter = new PythonSyntaxHighlighter(scriptWidget->document());

  connect (RunButton, SIGNAL( clicked() ), this, SLOT( runScript()) );

  QIcon icon;
  icon.addFile(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"window-close.png");
  actionClose->setIcon(icon);
  closeButton->setIcon(icon);

  QIcon iconRun;
  iconRun.addFile(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"arrow-right.png");
  RunButton->setIcon(iconRun);

  setWindowTitle(tr("%1 Python Interpreter").arg(TOSTRING(PRODUCT_NAME)));

  connect( actionClose , SIGNAL(triggered() ) , this, SLOT(hide()) );
  connect( closeButton , SIGNAL(clicked()   ) , this, SLOT(hide()) );
  closeButton->setFocus();

#ifdef PYTHON_ENABLED
  pythonInfo->setAlignment(Qt::AlignLeft);


  pythonInfo->append("Each module is automatically loaded by the core with the name given below.");
  pythonInfo->append("An instance for each plugin is automatically generated with a lower case name of the module (E.g. the plugin Move will provide an instance move).\n");
  pythonInfo->append("Available plugins with Python support:\n");

  QStringList pythonPlugins = getPythonPlugins();

  for ( int i = 0 ; i < pythonPlugins.size() ; ++i ) {
    pythonInfo->append("Module " + pythonPlugins[i] + "\t\t providing instance " + pythonPlugins[i].toLower());
  }

  PythonInterpreter* interpreter = PythonInterpreter::getInstance();
  if ( OpenFlipper::Options::gui() )
    connect(interpreter,SIGNAL(log(Logtype,QString)) , this, SLOT(slotLocalLog(Logtype,QString)));


  QTextEdit* coreInfo = new QTextEdit( infoTab );
  moduleTab->addTab(coreInfo,"Core");
  QString coreDoc =  interpreter->runScriptOutput("import pydoc ;import openflipper;html = pydoc.HTMLDoc();object, name = pydoc.resolve(openflipper);page = html.page(pydoc.describe(object), html.document(object, name));print(page)");
  coreInfo->setHtml(coreDoc);

  // Collect all Python Documentation for the OpenFlipper Plugin Modules
  for ( int i = 0 ; i < pythonPlugins.size() ; ++i ) {
    QTextEdit* edit = new QTextEdit( moduleTab );
    moduleTab->addTab(edit,pythonPlugins[i]);
    QString data =  interpreter->runScriptOutput("import pydoc ;import "+pythonPlugins[i]+";html = pydoc.HTMLDoc();object, name = pydoc.resolve("+pythonPlugins[i]+");page = html.page(pydoc.describe(object), html.document(object, name));print(page)");
    edit->setHtml(data);
  }

#endif

}



void PythonWidget::runScript() {

#ifdef PYTHON_ENABLED
  PythonInterpreter* interpreter = PythonInterpreter::getInstance();
  interpreter->runScript(scriptWidget->toPlainText());
#else
  std::cerr << "OpenFlipper is not compiled with python support. Unable to execute script!" << std::endl;
#endif

}

void PythonWidget::slotLocalLog(Logtype _type ,QString _logString) {

  switch (_type) {
    case LOGINFO:
      pythonOutput->setTextColor( QColor(Qt::darkGreen) );
      break;
    case LOGOUT:
      pythonOutput->setTextColor( QPalette{}.windowText().color() );
      break;
    case LOGWARN:
      pythonOutput->setTextColor( QColor(160,160,0) );
      break;
    case LOGERR:
      pythonOutput->setTextColor( QColor(Qt::red) );
      break;
    case LOGSTATUS:
      pythonOutput->setTextColor( QColor(Qt::blue) );
      break;
  }

  pythonOutput->append(_logString);
}






