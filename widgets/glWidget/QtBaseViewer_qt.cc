/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/




// QOpenGL headers and glew are in conflict,
// so implement functions that make use of QOpenGL classes in separate file


//=============================================================================
//
//  CLASS glViewer - IMPLEMENTATION
//
//=============================================================================


//== INCLUDES =================================================================

#include "QtBaseViewer.hh"

#include <OpenFlipper/common/GlobalOptions.hh>
#include <QOffscreenSurface>

#include <QOpenGLContext>
#include <QOpenGLDebugLogger>


#include <QOpenGLWidget>
#include <QOpenGLFramebufferObject>

//== NAMESPACES ===============================================================



//== IMPLEMENTATION ==========================================================



void glViewer::swapBuffers() {

  glWidget_->context()->swapBuffers(glWidget_->context()->surface());

}

//-----------------------------------------------------------------------------

void glViewer::startGLDebugLogger()
{

  if (OpenFlipper::Options::debug())
  {
    delete glDebugLogger_;
    glDebugLogger_ = new QOpenGLDebugLogger(this);
    if (glDebugLogger_->initialize())
    {
      connect(glDebugLogger_, SIGNAL(messageLogged(QOpenGLDebugMessage)), this, SLOT(processGLDebugMessage(QOpenGLDebugMessage)));
      glDebugLogger_->startLogging(QOpenGLDebugLogger::SynchronousLogging);
    }
  }

}

//-----------------------------------------------------------------------------

void glViewer::deleteGLDebugLogger()
{
  makeCurrent();
  delete glDebugLogger_;
}

//-----------------------------------------------------------------------------

void glViewer::processGLDebugMessage(const QOpenGLDebugMessage& msg)
{
  if (msg.severity() & QOpenGLDebugMessage::HighSeverity)
    std::cerr << msg.message().toStdString() << std::endl;

  // also catch deprecated function calls in core profile
  else if (!glstate_->compatibilityProfile() && msg.type() == QOpenGLDebugMessage::DeprecatedBehaviorType)
    std::cerr << msg.message().toStdString() << std::endl; 
}


//-----------------------------------------------------------------------------

void
glViewer::copyToImage( QImage& _image,
		       unsigned int _l, unsigned int _t,
		       unsigned int _w, unsigned int _h,
			     GLenum /* _buffer */ )
{

  _image = glWidget_->grabFramebuffer().copy(_l, _t, _w, _h).convertToFormat(QImage::Format_RGB32);
}

//-----------------------------------------------------------------------------

void glViewer::makeWidgetCurrent()
{
  glWidget_->makeCurrent();
}

//-----------------------------------------------------------------------------

bool glViewer::createQFBO(QOpenGLFramebufferObject*& ptr, GLuint* _handle, int _width, int _height, int* _samples)
{
  QFramebufferObjectFormat format;
  format.setInternalTextureFormat(GL_RGBA);
  format.setTextureTarget(GL_TEXTURE_2D);
  if(*_samples > -1) //use -1 to indicate, that sample count is bit set
  {
    // set the attachments as in the standard rendering
    format.setAttachment(QFramebufferObject::CombinedDepthStencil);
    // 16 samples per pixel as we want a nice snapshot. If this is not supported
    // it will fall back to the maximal supported number of samples
    format.setSamples(*_samples);
  }

  QFramebufferObject* fb;
  fb = new QFramebufferObject(_width, _height, format);
  ptr = fb;
  if(fb->isValid())
  {
    *_handle = fb->handle();
    *_samples = fb->format().samples(); // store the actual samples qt uses
    return true;
  }
  else
  {
    *_handle = 0;
    return false;
  }
}

//-----------------------------------------------------------------------------

void glViewer::blitQFBO(QOpenGLFramebufferObject* _ptr1, const QRect& _size1, QOpenGLFramebufferObject* _ptr2, const QRect& _size2)
{
  QFramebufferObject::blitFramebuffer(_ptr1, _size1, _ptr2, _size2);
}

//-----------------------------------------------------------------------------

bool glViewer::bindQFBO(QOpenGLFramebufferObject* _ptr)
{
    return _ptr->bind();
}

//-----------------------------------------------------------------------------

bool glViewer::QFBOResized(QOpenGLFramebufferObject* _ptr1)
{
    return _ptr1->size() != QSize(glWidth(), glHeight());
}

//-----------------------------------------------------------------------------

void glViewer::deleteQFBO(QOpenGLFramebufferObject* _ptr)
{
  delete _ptr;
}

//=============================================================================
//=============================================================================

