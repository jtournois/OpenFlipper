/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/



#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/Core/Core.hh>

#include <QtWidgets>

#include <QDirModel>

#include <map>

#include <OpenFlipper/common/FileTypes.hh>


class LoadWidget : public QFileDialog
{
  Q_OBJECT
  public:
    LoadWidget(std::vector<fileTypes>& _supportedTypes , QWidget *parent = 0 );
    ~LoadWidget();

  signals:
    void load(QString _filename, int _pluginID);
    void loadFiles(QStringList _filenames, IdList _pluginIds);
    void save(int     _id, QString _filename, int _pluginID);
    void save(IdList _ids, QString _filename, int _pluginID);
    
  private slots:
    void slotSetPluginForExtension(QString _extension, int _pluginId );
    void slotSetLoadFilters();
    void slotSetSaveFilters(DataType _type);

  private:
    IdList ids_;

    bool loadMode_;
    QStringList lastPaths;
    std::vector<fileTypes>& supportedTypes_;
    
  public:
    int showLoad();
    int showSave(int     _id, QString _filename);
    int showSave(IdList _ids, QString _filename);
    
    bool validFilename();
    /// returns true, if the directory of the specified filename (user input via widget) exists
    bool dirExists();
    
  public slots :
    virtual void accept();
    
  private :
    void loadFile();
    void saveFile();
    
    /// checkbox for option displaying
    QCheckBox* optionsBox_;

    std::map< QString, int > pluginForExtension_;
    
};

